# Le projet MATOU 

Le but du projet Matou est de réaliser un service de discussions et d'échanges de fichiers.

## Principe de l'application

Les clients se connectent à un serveur. Chaque client connecté est identifié par un pseudonyme. Le serveur doit garantir que deux clients ne peuvent pas avoir le même pseudonyme. Une fois connectés et identifiés par un pseudonyme, les clients peuvent:

    1. envoyer des messages qui seront transmis à tous les clients connectés.
    2. envoyer des messages privés et des fichiers à un autre utilisateur.

Par rapport à un serveur de chat standard (type IRC), la particularité de ce projet est que tous les messages privés et les fichiers sont envoyés par une connection directe entre les deux clients: le serveur permet juste dans ce cas de s'échanger les adresses des clients.

Le protocole devra permettre aux clients de faire des demandes de communication privée et d'accepter/refuser la communication privée. Une fois que la communication privée entre deux clients est établie, l'envoi/réception de fichiers ne doit pas bloquer les messages entre ces deux clients. 

## Version un peu imagé

Alice qui envoie un message à la salle qui retransmet aux autres.
Voir [Ici](protocole.md) pour plus d'informations.

```mermaid
graph TD;

Alice-->|Message|Serveur;
Serveur-->|Message|Bob;
Serveur-->|Message|Marc;

```