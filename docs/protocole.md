# Protocole

## GITLAB PUBLIC 
BUYUK / MPATI
https://gitlab.com/bali0x4d/M1_IF_PROJET_MATOU.git

## VERSION / DATE
V0.1 07/03/2018 ( esquisse )
V0.2 14/03/2018 ( échange fichier / requêtes )
V0.3 10/04/2018 ( Ajout liste utilisateur )

Ceci est une esquisse , elle sera amélioré au fur et à mesure.
Elle sera présentée le 14/03/2018.

## Sommaire

1. [Code Requêtes](#req)
2. [Client => Connection/Deconnection](#connexion)
3. [Client => Lister salle](#liste)
4. [Client => Envoie Message salle](#e_msg_pub)
5. [Client => Réception Message salle](#r_msg_pub)
6. [Client => Demande de Conversation privée](#d_msg_pri)
7. [Client => Demande d'envoie de fichier](#d_env_fic)
8. [Client => Lister les utilisateurs](#lst_usr)


## Requête

Ici les requêtes seront un mélange de TCP.

Un paquet aura une taille maximum de 1024 Bytes quoi qu'il arrive.

Toutes les requêtes seront encodées en UTF-8.

D'autre spécifications seront ajoutés au fur et à mesure.

## <a name="req"></a>Code requête


| NUMERO/CODE (CR) | SIGNIFICATION|
|---|---|
|0| Authentification|
|1| Autorisation|
|2| Demande Salle|
|3| Liste Salle |
|4| Connexion Salle|
|5| Réponse Salle|
|6| Envoie Message Unique |
|7| Envoie Message Multiple |
|8| Fin Message Multiple |
|9| Réception message unique |
|10| Réception message multiple |
|11| Fin Réception message multiple |
|12| Déconnexion salle/conv privée | 
|13| Déconnexion serveur / Fin Appli | 
|14| Demande conversation privée par le client|
|15| Demande d'envoie de fichier |
|16| Réception fichier |
|17| Fin fichier |
|18| Information correspondant conversation privée |
|19| Réponse à une requête |
|20| Demande de conversation privée par le serveur |
|19| Refus à une requête de connexion privée |
|21| Liste des utilisateurs sur le serveur public |
|22| Authentification privée |
|23| PortPrivé |
|24| Requête fichier |

## Côté client

### <a name="connexion"></a>Connexion / Déconnexion

Ici pour se connecter , le client ( Ici Alice ) devrat envoyer une requête de connexion.
Dans sa requête elle y stipulera un nom de connexion ( son pseudo ).

Requête.

|  Code requête | Taille Message  |  Pseudo | 
|---|---|---|---|---|
| 0 | int  | String UTF-8  |


Réponse.

En retour elle recevra un jeton d'authentification, qui lui accordera ou non l'accés au serveur.

Si réussi , elle aura accés au serveur.
Sinon on affichera le message d'erreur correspondant au code d'erreur.

| Code Requête | Etat | Token |
|---|---|---|---|---|
| 1  | int | long | 

| Code Retour | signification |
|---|---|
| 0 |  Connection accepté |
| 1 | Erreur pseudo existant |
| 2 | Erreur serveur occupé |
| 3 | Erreur serveur pleins |
| 4 | Erreur inconnu |

Le Token est unique à chaque utilisateur.

```mermaid
graph LR;

Alice-->|PaquetDeConnexion|Serveur;
Serveur-->|PaquetAutorisation|Alice;

```

### Deconnexion salle / conversation privée

Si Alice veut quitter la salle ou une conversation privée.
Lorsque Alice aura choisit sa salle. Elle demandera à se déconnecter et si tout va bien , elle sera déconnectée de cette salle. 

| Code Requête  |  Token |
|---|---|---|---|---|
| 12 | long |

### Deconnexion du serveur

Si Alice veux se déconnecter du serveur, elle demandera à se déconnecter et si tout va bien , elle sera déconnectée du serveur et l'application se fermera.

| Code Requête  |  Token |
|---|---|---|---|---|
| 13 | long |

### <a name="liste"></a>Lister les salles

Lorsque qu'un utilisateur aura réussi sa connexion au serveur , il aura accés à un menu. De la il choisira la salle ou il voudra se connecter.

Requête.

On envoie la demande.

| Code Requête  |  Token |
|---|---|---|---|---|
| 2 | long |


Et on reçoit si tout va bien, la liste des salles , le nombre de connectés et les places disponibles.

| Code Requête  | NombreSalle | SalleX | Nb Personne In | NbPersonne Max | ...|
|---|---|---|---|---|---|
| 3 | int | int | int | int | ...|

La liste se décompose comme suit.

3 int par salle,un pour le numéro de la salle, un  deuxième pour le nombre de personne dans la salle et un pour le nombre de maximum dans la salle.


```mermaid
graph LR;

Alice-->|PaquetSalle|Serveur;
Serveur-->|PaquetListeSalle|Alice;

```


## <a name="conn_salle"></a>Se connecter à une salle

Lorsque Alice aura choisit sa salle. Elle demandera à se connecter et si tout va bien , elle sera connecté sur cette salle. 

Requête.

On envoie la demande.

|  Code Requête  |  Token | num salle |
|---|---|---|
|  4 | long | int |


Et on reçoit si tout va bien, la réponse. Si 0 erreur sinon OK.

| Code Requête  | Etat |
|---|---|
| 5 | int |

| Code Retour | Siginification |
|---|---|
| 0 |  Connecté dans la salle |
| 1 |  Salle pleine |
| 2 | Erreur inconnu |


```mermaid
graph LR;

Alice-->|PaquetSalleConnexion|Serveur;
Serveur-->|PaquetSalleConnecté|Alice;

```

## <a name="e_msg_pub"></a>Envoie d'un message ( version public )

### Cas 1 Le message tient dans un paquet 

Alice envoie un message dans sa salle et elle tient sur un seul paquet.

|  Code Requête  |  Token | Taille Message |Message |
|---|---|---|---|
| 6 | int | int |Message UTF-8 |

### Cas 2 Le message tient sur plusieurs paquets

En TCP, pas besoin de gérer les collisions/ordres d’arrivés. 
On n'a pas besoins de connaitre la taille du message ,on sait qu'il fait tout le buffer.

|  Code Requête  |  Token | Message |
|---|---|---|
|  7 | long | Message encodé UTF-8 |


Et le dernier

|  Code Requête  |  Token | Taille Message |Message |
|---|---|---|---|
|  8 | long | int |Message encodé UTF-8 |


```mermaid
graph LR;

Alice-->|PaquetMessageUnique|Serveur;

```

ou bien un message de taille 3

```mermaid
graph LR;

Alice-->|PaquetMessage1|Serveur;
Alice-->|PaquetMessage2|Serveur;
Alice-->|PaquetMessage3Fin|Serveur;

```

## <a name="r_msg_pub"></a>Réception d'un message ( version public )

Cela ressemble beaucoup à l’envoie de message. Seul les codes requêtes changent.

### Cas 1 Le message tient dans un paquet 

Alice reçoit un message de sa salle et il tient sur un seul paquet.

|  Code Requête  | Taille Message | Message |
|---|---|---|
| 9 | int | Message UTF-8 |

### Cas 2 Le message tient sur plusieurs paquets

En pas besoin de gérer pas les collisions/ordres d’arrivés.
On n’a pas besoins de connaitre la taille du message ,on sait qu’il fait tout le buffer dans un premier temps.

|  Code Requête  | Message |
|---|---|---|
| 10 | Message encodé UTF-8 |


Et le dernier qui annonce la fin.

|  Code Requête  | Taille Message | Message |
|---|---|---|
|  11 | int | Message encodé UTF-8 |


```mermaid
graph LR;

Serveur-->|PaquetMessageUnique|Alice;

```

ou bien un message de taille 3

```mermaid
graph LR;

Serveur-->|PaquetMessage1|Alice;
Serveur-->|PaquetMessage2|Alice;
Serveur-->|PaquetMessage3Fin|Alice;

```

## <a name="lst_usr"></a> Lister les utilisateurs sur le serveur public

On envoie simplement une requête comme indiqué.

| CR |
|---|
| 21 | 

Et on reçoit sous forme de message classique par le serveur , la liste des utilisateurs pré formaté et affiché sur la console.


## <a name="d_msg_pri"></a>Demande de conversation privé

Si Alice souhaite parler à Bob en privé, elle envoit un buffer au serveur contenant le code requête demandant l'ouverture d'une conversation privé.

Ce buffer contient un code requête, la taille du pseudo de Bob et le pseudo de Bob encodé.

|  Code Requête  | Taille Message | Pseudonyme |
|---|---|---|
|  14 | int | Pseudonyme encodé UTF-8 |

Le serveur envoit un simple message à Bob, lui proposant l'ouverture d'une conversation privée avec le pseudo de la personne qui demande.

|  Code Requête  | Taille Message | Pseudonyme |
|---|---|---|
| 20 | int | Message UTF-8 |

Bob répond oui ou non avec un buffer contenant un entier avec un 0 ou un 1 avec le nom de l'utilisateur au serveur.

|  Code Requête  | Entier | Entier | Pseudo UTF-8 |
|---|---|---|---|
|  19 | 0 - 1 | int | Pseudo du demandeur |

Puis le serveur renvoie le même message à Alice.

|  Code Requête  | Entier | Entier | Pseudo UTF-8 |
|---|---|---|---|
|  19 | 0 - 1 | int | Pseudo demandé |

Enfin le serveur envoit aux deux pairs leur informations (Ip etc) afin qu'ils puissent ce connecter
Ce message sera de la forme PSEUDO:TOKEN:IPorAddress:Port  ( ici le séparateur est un double point)

|  Code Requête  | Taille Message | Message |
|---|---|---|
| 18 | int | Message UTF-8 | 

Sinon le serveur renvoit un CR 19 pour signifier le refus à Alice avec le pseudo de la personne demandée.

|  Code Requête  | Entier | Entier | Pseudo |
|---|---|---|---|
|  19 | 0 | int | pseudo en UTF8 |

#### CAS REPONSE POSITIVE

Il y a Bob , Alice et le serveur.

```mermaid
graph LR;

Alice-->|ConvPrivéAvecBob|Serveur;
Serveur-->|DemandeABob|Bob;
Bob-->|BobDitOui|Serveur;

```
Puis
```mermaid
graph LR;

Serveur-->|PaquetInfo|Bob;
Serveur-->|PaquetInfo|Alice;

```

Les deux peuvent se parler.

```mermaid
graph LR;

Alice-->|PaquetMessage|Bob;
Bob-->|PaquetMessage|Alice;

```

Bien sur entre deux connexions , on demande que les deux s'authentifie.
Grâce au code 22
| CR | int | int | message pseudo utf-8 |
| 22 | token | size | message |

#### CAS REPONSE NEGATIVE

Alice demande et Bob refuse.

Il y a Bob , Alice et le serveur.

```mermaid
graph LR;

Alice-->|ConvPrivéAvecBob|Serveur;
Serveur-->|DemandeABob|Bob;
Bob-->|BobDitNon|Serveur;

```
Puis
```mermaid
graph LR;

Serveur-->|PaquetRefus|Alice;

```

## <a name="d_env_fic"></a> Demande d'envoi de fichier

Lorsqu'Alice souhaite envoyer un fichier à Bob, Un buffer contenant le code requête et le nom du fichier sont envoyés à Bob.

|  Code Requête  | Taille Message | Nom du fichier | 
|---|---|---|
|  15 | int | Nom encodé en UTF-8 |

Bob répond oui ou non avec un buffer contenant un entier avec un 0 ou un 1

|  Code Requête  | Entier | Entier | Nom du Fichier |
|---|---|---|---|
| 24 | 0 - 1 | int | Nom du fichier |

Le fichier est envoyer par un buffer simple (code requète, octets du fichier).
Pas besoin de la taille ici car on sait qu'il fait tout le buffer.
Si un fichier fait moins de la taille d'un paquet on passe simplement à l'étape suivante.

|  Code Requête  | Fichier | 
|---|---|
|  16 | Octets du fichier |

Une fois atteint le dernier paquet à envoyer, Bob envoi un dernier buffer contenant la fin du fichier annonçant le transfert complet du fichier.

|  Code Requête  | Taille Message | Fin du fichier | 
|---|---|---|
|  17 | int | Octets du fichier |


Il y a Bob et Alice en conversation privée.

```mermaid
graph LR;

Alice-->|EnvoiePaquet|Bob;
Bob-->|BobDitOui|Alice;

```
Puis le transfert commence
```mermaid
graph LR;

Alice-->|PaquetFichier|Bob;

```
Et se termine part 

```mermaid
graph LR;

Serveur-->|FinFichier|Bob;

```