# Ce qu'il reste à faire
**D'autres points s'ajouteront au fur et à mesure**

- [x] Créer le dépôt
- [x] Expliquer le projet
- [ ] Créer le protocole 
> - [ ] Connexion
> - [ ] Envoie de message
> - [ ] Réception de message
> - [ ] Conversation privé
> - [ ] Envoie de fichier
- [ ] Créer Appli Client
- [ ] Créer Appli Serveur