# Projet MATOU 2018 M2 IF JAVA RÉSEAUX
## Par MPATI Landry / BUYUK Baran

### Changelog
#### Version 0.1  du  07/03/2018

> Y'a quoi dans cette version 

 - Création du protocole de base
 - Explication et exemples 

**Ceci est la version bêta du protocole que l'on va utiliser dans notre projet. Elle sera améliorée au fur et à mesure de l'avancement du projet. Chaque amélioration du protocole sera expliqué ici** 

### Par ou commencer 

1. [Le projet ](docs/projet.md)
2. [Le protocole](docs/protocole.md)
3. [L'appli Client](docs/client.md)
4. [L'appli Serveur](docs/serveur.md)

### Le coin du dev
 
1. [La doc du projet ( A Venir )]()
2. [To Do](docs/todo.md)