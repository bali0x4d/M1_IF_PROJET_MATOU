package app.client.unique;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Objects;
import java.util.Scanner;

import app.client.client.Client;
import app.client.langue.Lang;
import app.client.rfc.CR;
import app.client.test.Debug;


/**
 * @author bali
 *Main Information for server connectiuon
 */
public class Connect {
	
	/**
	 * readear
	 * 
	 */
	private static Scanner in = null;
	
	/**
	 * url 
	 */
	private static String url = null;
	/**
	 * port
	 */
	private static int port = 0;
	/**
	 * InetSocketAddress
	 */
	private static InetSocketAddress isa = null;
	/**
	 * SocketChannel
	 */
	private static SocketChannel sc = null;
	/**
	 * closed , if read/write close , leave connection
	 */
	private static boolean closed = false;
	/**
	 * token for user
	 */
	private static int token = -1;
	/**
	 * Name of user
	 */
	private static String name = null;
	// sync it 
	private static Object sync = new Object();
	

	private static int nbRoom = -1;
	
	public static Scanner getScanner() {
		return in;
	}
	
	public static void setScanner(Scanner in1) {
		in = in1;
	}	
	
	/**
	 * Set Name for user
	 * @param n the name
	 */
	public static void setName(String n) {
		 Objects.requireNonNull(n);
		 Connect.name = n;
		 
	}
	
	
	public static int inRoomNb() {
		return nbRoom;
	}
	
	public static void connectRoom(int nbR) {
		nbRoom = nbR;
	}
	
	/**
	 * Parse user input and try to resolve into InetSocketAddress
	 * @param userInput url/ip port
	 * @return boolean , if true it's good otherwise false...
	 */
	public static boolean setConnection(String userInput) {
			
		String[] tmp = userInput.split(" ");
		
		if( tmp.length != 2 ) {
			return false;
		}
		
		System.out.println(tmp[0]+" / " +tmp[1]);
		
		url = tmp[0];
		port = Integer.parseInt(tmp[1]);
		
		if( port < 0 || port > 65535) {
			
			System.out.println(Lang.getTrans("errorPort"));
			return false;
			
		}
		
		isa = new InetSocketAddress(url,port);
		
		if( isa.isUnresolved()) {
			
			System.out.println(Lang.getTrans("errorUrl"));
			return false;
		}
		
		return true;
		
		
	}
	
	
	
	/**
	 * try to open tcp connection , he closed the last connection if it exist
	 * @return boolean if failed return false else true
	 */
	public static boolean openConnection() {
		try {
			// close it before reoppening ...
			if( Connect.closed) {
				Connect.closeConnection();
			}
			sc = SocketChannel.open();	
			sc.connect(isa);
			resetClosed();
			return true;
		} catch (IOException e) {
			System.out.println(Lang.getTrans("errorSC"));
			return false;
		}
		
	}
	
	/**
	 * close the connection silently
	 */
	private static void closeConnection() {
		
		try {
			// close my way not him
			sc.close();
		} catch (IOException e) {
			// do nothing 
		}
		
		
		
	}
	
	/**
	 * Get the current socket channel
	 * @return SocketChannel 
	 */
	public static SocketChannel getSC() {
		return sc;
	}
	
	/**
	 * if write or read mode is closed or not
	 * @return boolean
	 */
	public static boolean isClosed() {
		
		synchronized(sync){
			return closed;
		}
	}
	
	
	/**
	 * reset closed value to false
	 */
	public static void resetClosed() {
		synchronized(sync) {
			closed = false;
		}		
		
	}
	
	/**
	 * set value of closed to true
	 */
	public static void closedByServer() {
		
		synchronized(sync) {
			closed = true;
		}
	}

	
	/**
	 * try to connect to the server , get a token etc
	 * @return boolean
	 */
	public static boolean askConnection() {
		
		try {
			sc.configureBlocking(false);
		} catch (IOException e1) {
			//skip
		}
		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.putInt(CR.Authentification.ordinal());
		
		
		ByteBuffer pseudo = Client.UTF8_CHARSET.encode(Connect.getName());
		
		
		tmp.putInt(pseudo.limit());
		
		tmp.put(pseudo);
		
		tmp.flip();		
		
		if( !writeFullyWithTimeOut(sc,tmp,10000)) {
			System.out.println(Lang.getTrans("fatalError")+ "Error 2");
			return false;
		}

		
		tmp.clear();
		tmp.limit(3*Integer.BYTES);
		
		// try to read with timeout
			
			if( !readFullyWithTimeOut(sc,tmp,10000)) {				
				// error closed ?
				System.out.println(Lang.getTrans("closeConn")+ "Error 4");
				return false;
			}
					
		
		tmp.flip();
		
		int cr = tmp.getInt();
		int etat = tmp.getInt();
		int tk = tmp.getInt();
		
		
		if( cr == CR.Autorisation.ordinal()) {
			
			if( etat != 0) {
				System.out.println(Lang.getTrans("connRefused")+ "Error 3 expected 0 got "+ cr);
				return false;
			}
			token = tk;
			Debug.printDebug("Token get and it's " + tk );

			
		}
		else {
			System.out.println(Lang.getTrans("wrongCR")+ "Error 4: Expected "+CR.Autorisation.ordinal() + " get " + cr);
			return false;
		}
		
		Debug.printDebug("connection successfull");
		//get back blocking mode
		try {
			sc.configureBlocking(true);
		} catch (IOException e) {
			// do nothing 
			return false;
		}
		return true;
	}

	/**
	  * Fill the workspace of the Bytebuffer with bytes write from sc with timeout
	  * socketchannel have to be in nonblockingmode
	  *with timeout before fail
	  *
	  * @param sc
	  * @param
	  * @param timeOut 
	  * @return false if write returned -1 at some point and true otherwise
	  */
	static boolean writeFullyWithTimeOut(SocketChannel sc, ByteBuffer bf,int timeOut){
			
		if(sc.isBlocking()) {
			
			System.out.println(Lang.getTrans("fatalError")+ "Error 8");
			return false;
		}
		
		
		
		long time = System.currentTimeMillis();
			
			
			int write;
			try {
				write = sc.write(bf);
			} catch (IOException e) {				
				return false;
			}
			
			while(write != -1 && (System.currentTimeMillis()-time) < timeOut) {
				
				try {
					write = sc.write(bf);
				} catch (IOException e) {
					return false;
				}
				
				if( !bf.hasRemaining()) {
					return true;
				}
				
				
			}
			
		    return false;
	}
	
	/**
	  * Fill the workspace of the Bytebuffer with bytes read from sc with timeout
	  * socketchannel have to be in nonblockingmode
	  *with timeout before fail
	  *
	  * @param sc
	  * @param
	  * @param timeOut 
	  * @return false if read returned -1 at some point and true otherwise
	  */
 	static boolean readFullyWithTimeOut(SocketChannel sc, ByteBuffer bf,int timeOut){
 			
 		if(sc.isBlocking()) {
 			
 			System.out.println(Lang.getTrans("fatalError")+ "Error 8");
 			return false;
 		}
 		
 		
 		
 		long time = System.currentTimeMillis();
 			
 			
 			int read;
			try {
				read = sc.read(bf);
			} catch (IOException e) {				
				return false;
			}
 			
 			while(read != -1 && (System.currentTimeMillis()-time) < timeOut) {
 				
 				try {
					read = sc.read(bf);
				} catch (IOException e) {
					return false;
				}
 				
 				if( !bf.hasRemaining()) {
 					return true;
 				}
 				
 				
 			}
 			
 		    return false;
 	}

	/**
	 * Get user name
	 * @return String
	 */
	public static String getName() {
		return Connect.name;
	}

	/**
	 * get token of user
	 * @return int
	 */
	public static int getToken() {
		return token;
	}

	
	
}
