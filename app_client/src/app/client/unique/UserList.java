package app.client.unique;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import app.client.client.Client;
import app.client.rsc.Message;
import app.client.server.State;
import app.client.server.UserContext;
import app.client.server.UserContext.FilesOP;
import app.client.test.Debug;


/**
 * @author bali
 *This is for the private connection 
 */
public class UserList {
	
	/**
	 * user of private conv 
	 */
	private static HashMap<String,UserContext> privConv = new HashMap<>();
	
	
	private static Object lock = new Object();
	
	
	/**
	 * check user info 
	 * @param token token
	 * @param name name
	 * @return State 
	 */
	public static State authUserFromServer(int token , String name) {

		synchronized(lock) {
			Debug.printDebug("name :"+name+":");
			
			UserContext tmp = privConv.get(name);

			
			Debug.printDebug("contains or not" + privConv.containsKey(name));
			
			
			
			
			if( tmp == null) {
				Debug.printDebug("Its null why");
				return State.REFUSED;

			}


			if( tmp.getToken() == token ) {

				return tmp.getState();
			}
			else if( tmp.getToken() == -1 ) {

				return State.PENDING;
			}
			Debug.printDebug("why di you do this to me "+ tmp.getToken() + " / " + token);
			return State.REFUSED;
		}
	}
	
	/**
	 * Receive query to user for receiving file 
	 * @param nameUser name of user
	 * @param fileName file name
	 */
	public static void receiveQueryForFile(String nameUser, String fileName) {
		
		
		synchronized(lock) {
			UserContext usr = privConv.get(nameUser);
			
			if( usr == null || usr.getState() != State.OPEN) {
				System.out.println("Cant get files");
				return;
			}

			usr.rForFiles(fileName);
			
		}
		
		
	}	
	
	
	/**
	 * Send query to user for receiving file 
	 * @param nameUser name of user
	 * @param fileName file name
	 */
	public static void sendQueryForFile(String nameUser, String fileName) {
		
		
		synchronized(lock) {
			UserContext usr = privConv.get(nameUser);
			
			if( usr == null || usr.getState() != State.OPEN) {
				System.out.println("Cant send files");
				return;
			}

			usr.qForFiles(fileName);
			
		}
		
		
	}
	
	
	/**
	 * Get state for user
	 * @param name string name for user
	 * @return State or null
	 */
	public static State getStateOfUser(String name) {
		
		if( name == null ) {
			return null;
		}
		
		synchronized(lock) {
			
			UserContext tmp = privConv.get(name);
			
			if( tmp == null ) {
				return null;
			}
			
			return tmp.getState();
			
		}
		
		
	}
	
	/**
	 * Set an user but dont connect 
	 * @param name name of user
	 */
	public static void setUser(String name) {
		
		
		synchronized(lock) {
			Debug.printDebug("Adding "+ name + "->" +name.length());
			privConv.computeIfPresent(name, (k,v) -> {
				return UserContext.initUser(name);
			});
			
			privConv.computeIfAbsent(name, (k) ->{			
				return UserContext.initUser(name);			
			});
			
			Debug.printDebug("Added exist ? " + privConv.containsKey(name) );
		}
		
	}
	
	/**
	 * Disconnect from client... 
	 * @param name
	 */
	public static void removeUser(String name) {
		
		synchronized(lock) {
			Debug.printDebug("Removingf user " + name);
			UserContext tmp = privConv.get(name);
			if( tmp == null) {
				return;
			}
			
			tmp.closeConnection();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
			privConv.remove(name);
		}
		
	}
	
	/**
	 * Change state of user
	 * @param name name of user
	 * @param st state the new
	 */
	public static void changeUserState(String name,State st) {
		
		Debug.printDebug("Changing state of "+ name + "to " + st);
		UserContext tmp = privConv.get(name);
		
		if( tmp != null ) {
			tmp.changeState(st);
		}
		
	}
	
	/**
	 * Message to send to user
	 * @param msg list of message to send
	 */
	public static void addMessage(String name,List<Message> msg) {
		
		if(msg == null || name == null) {
			return;
		}
		
		synchronized(lock) {
			
			UserContext tmp = privConv.get(name);
			
			if( tmp == null) {
				return;
			}
			
			msg.forEach(m -> tmp.putMessage(m));
			
		}
		
	}
	
	
	/**
	 * Check if conversation is accepted
	 * @param msg message
	 */
	public static void answearFrom(Message msg) {
		ByteBuffer tmp = msg.getBuffer().flip();
		int state = tmp.getInt();
		String name =  Client.UTF8_CHARSET.decode(tmp).toString();
		
		if( state == 1) {
			System.out.println("ACCEPTED CONV FROM " + name);
			UserList.changeUserState(name, State.PENDING);
		}
		else {
			System.out.println("REFUSED CONV FROM " + name);	
			removeUser(name);
		}
		
		
	}
	
	/**
	 * Start user from message 
	 * @param msg the packet for message 
	 * @param trendingMsg 
	 * @param msgToRead 
	 */
	public static void startUser(Message msg, BlockingQueue<Message> msgToRead) {
		
		String[] inf = Client.UTF8_CHARSET.decode(msg.getBuffer().flip()).toString().split(":");
		
		Arrays.stream(inf).forEach(s-> System.out.println(s));
		
		if( inf.length != 4) {
			System.out.println("error in information , remove user ??? ");
			return;
		}
		
		
		String pseudo = inf[0];
		String token = inf[1];
		String ip = inf[2];
		String port = inf[3];
		
		UserContext tmp = UserList.getUser(pseudo);
		
		if( tmp == null) {
			System.out.println("error in information , user dont exist ??? ");
			return;
		}
		
		tmp.changeState(State.WAITINGFORAUTH);
		if( tmp.connectUser(pseudo, Integer.valueOf(token), ip, Integer.valueOf(port),msgToRead)) {
			System.out.println("Connecting to "+ pseudo);
		}
		else {
			System.out.println("Something wrong happened with "+ pseudo +"!!!");
		}
		 

		
	}
	
	
	/**
	 * get user object
	 * @param name name of user
	 * @return USerContext
	 */
	public static UserContext getUser(String name) {
		
		return privConv.get(name);
		
	}


	public static void initUser(Message msg) {
		
		String name = Client.UTF8_CHARSET.decode(msg.getBuffer().flip()).toString();
		
		if( privConv.containsKey(name)) {
			System.out.println("Already exist");
			return;
		}
		else {
			System.out.println(name + " want priv with you");
			
			setUser(name);
		
		}
		
	}

	public static void responseForFile(String name, String fileName, boolean value) {
		
		UserContext usr = privConv.get(name);
		
		if( usr == null ) {
			System.out.println("Wrong name");
		}
		
		usr.rspForFiles(fileName,value);
		
	}

	public static void sendResponseForFile(String name, String fileName, boolean value) {
		
		
		UserContext ctx = privConv.get(name);
		
		ctx.sendResponseForFiles(fileName,value);
		
	}

	public static void clearFiles(String user) {
		
		UserContext usr = privConv.get(user);
		
		if( usr != null) {
			usr.removeFiles();
		}
		
	}

	public static void stateFile(String user) {
		UserContext usr = privConv.get(user);
		
		if( usr != null) {
			FilesOP tmp = usr.getFiles();
			System.out.println("File: " + tmp.clearedName + " in " + tmp.st);
		}		
		
	}
	
	
}
