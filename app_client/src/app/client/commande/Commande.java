package app.client.commande;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import app.client.client.Client;
import app.client.langue.Lang;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.server.State;
import app.client.test.Debug;
import app.client.unique.Connect;
import app.client.unique.UserList;
import app.client.user.MessageUtils;


/**
 * Commande classe 
 * @author bbuyuk
 *
 */
public class Commande {
	
	private static HashMap<String,Integer> cmd = new HashMap<>();
	private static HashMap<String,String> cmdHelp = new HashMap<>();
		
	
	
	static {

		addCmd("quit",CommandeType.quit.ordinal());
		addCmd("name",CommandeType.name.ordinal());
		addCmd("help",CommandeType.help.ordinal());
		addCmd("user",CommandeType.user.ordinal());
		addCmd("luser",CommandeType.luser.ordinal());
		addCmd("priv",CommandeType.priv.ordinal());
		addCmd("room",CommandeType.room.ordinal());


	};
	
	
	private static void addCmd(String name, int id) {
		
		cmd.put("/"+name,id);
		cmdHelp.put("/"+name, "h"+name);
		
		
	}
	
	/**
	 * Get commande id
	 * @param user user input
	 * @return Integer id
	 */
	public static Integer getCmdId(String user) {
		
		Integer tmp = cmd.get(user);
		
		return tmp == null?getCmdIdWithOption(user):tmp;
			
	}
	
	/**
	 * This is for multiple value in a commande
	 * @param user user input
	 * @return Integer id of cmd
	 */
	public static Integer getCmdIdWithOption(String user) {
		
		String key = null ;
		Iterator<String> it = cmd.keySet().iterator();
		while( it.hasNext() ) {
			
			key = (String)it.next();
			
			if(user.contains(key)) {
				return cmd.get(key);
			}
			
		}
		
		return null;
		
	}
	
	/**
	 * Print Help
	 * @return String 
	 */
	public static String getHelp() {
		
		StringBuilder str = new StringBuilder();
	
		cmdHelp.forEach((k,v) -> {
			
			str.append(k +" ==> "+ Lang.getTrans(v) + "\n");
			
		});
		
		return str.toString();
	}
	
	
	
	/**
	 * @return msg to list all 
	 */
	public static Message listOfRoom() {
		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.clear();
		tmp.putInt(CR.DemandeSalle.ordinal());		
		
		return Message.buildMessage(CR.DemandeSalle.ordinal(), tmp, ConnectionType.PUBLIC, -1);
				
	}
	
	
	/**
	 * connect room to server
	 * @param number String of number
	 * @return msg
	 */
	public static Message connectToRoom(String number) {
		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.clear();
		tmp.putInt(CR.ConnexionSalle.ordinal());
		tmp.putInt(Integer.valueOf(number));
		
		Message msg = Message.buildMessage(CR.ConnexionSalle.ordinal(), tmp, ConnectionType.PUBLIC, -1);
		System.out.println(Lang.getTrans("numberConn")+number);
		Connect.connectRoom(Integer.valueOf(number));
		
		return msg;
		
	}

	/**
	 * Send request code to stop connection 
	 * @return Message 
	 */
	public static Message sendQuit(long token) {
		
		ByteBuffer tmp = ByteBuffer.allocate(Integer.BYTES + Long.BYTES);
		
		tmp.putInt(CR.DeconnexionServeurFinAppli.ordinal());
		tmp.putLong(token);
		return Message.buildMessage(CR.DeconnexionServeurFinAppli.ordinal(), tmp , ConnectionType.PUBLIC, -1);
	}

	/** 
	 * Send a message to get user list
	 * @param token user token
	 * @return Message
	 */
	public static Message userList(long token) {
		
		ByteBuffer tmp = ByteBuffer.allocate(Integer.BYTES);
		tmp.putInt(CR.ListerUtilisateurServeurPublic.ordinal());
		
		return Message.buildMessage(CR.ListerUtilisateurServeurPublic.ordinal(), tmp, ConnectionType.PUBLIC, token);
	}
	
	
	/** Analyse user input
	 * @param input userinput
	 * @return List of message
	 */
	public static List<Message> privAnalyse(String input){
		
		String[] parse = input.split(" ");
		List<Message> msg = new LinkedList<>();
		if( parse.length < 3) {
			return null;
		}
		
		switch(parse[1]) {
			
			case "-c":
				
				if( UserList.getStateOfUser(parse[2]) != null && UserList.getStateOfUser(parse[2]) != State.REFUSED) {
					System.out.println(Lang.getTrans("wait"));
					return null;
					
				}
				
				
				ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
				tmp.putInt(CR.DemandeConversationPriveeClient.ordinal());
				ByteBuffer name = Client.UTF8_CHARSET.encode(parse[2]);
				
				if( name.limit() > tmp.remaining()) {
					Debug.printDebug("NOM TROP LONG");
					return null;
				}
				
				tmp.putInt(name.limit());
				tmp.put(name);
				
				msg.add(Message.buildMessage(CR.DemandeConversationPriveeClient.ordinal(), tmp, ConnectionType.PUBLIC, -1));
				UserList.setUser(parse[2]);
				return msg;
			case "-d":
				UserList.removeUser(parse[2]);
				ByteBuffer dtmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
				dtmp.putInt(CR.DeconnexionSalleConvPrivee.ordinal());
				dtmp.putInt(Connect.getToken());
				
				List<Message> deco = new LinkedList<>();
				
				deco.add(Message.buildMessage(CR.DeconnexionSalleConvPrivee.ordinal(), dtmp, ConnectionType.PRIVATE, Connect.getToken()));
				UserList.addMessage(parse[2],deco);
				UserList.removeUser(parse[2]);
				return null;
			case "-m":
				
				if( parse.length < 4) {
					return null;
				}
				
				if( UserList.getStateOfUser(parse[2]) != State.OPEN) {
					System.out.println(Lang.getTrans("userNotCon"));
					return null;
				}
				Debug.printDebug("Sending message in private");
				StringBuilder str = new StringBuilder();
				for(int i=3; i < parse.length ; i++) {
					str.append(parse[i]).append(" ");
				}
				List<Message> msgTo = MessageUtils.createMessage(str.toString(), ConnectionType.PRIVATE, Connect.getToken());
				
				if( msgTo != null) {					
					UserList.addMessage(parse[2], msgTo);
				}
				
				return null;
			case "-l":
					List<Message> msgToList = new LinkedList<>();
					msgToList.add(Commande.userList(0));
					return msgToList;
			case "-a":
				
				if( State.INIT == UserList.getStateOfUser(parse[2])) {
					
					ByteBuffer atmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
					atmp.clear();
					atmp.putInt(CR.ReponseRequeteConvPrivee.ordinal());
					atmp.putInt(1);
					ByteBuffer aname = Client.UTF8_CHARSET.encode(parse[2]);
					int size = aname.limit();
					atmp.putInt(size);
					atmp.put(aname);
					
					msg.add(Message.buildMessage(CR.ReponseRequeteConvPrivee.ordinal(), atmp, ConnectionType.PUBLIC, -1));
					UserList.changeUserState(parse[2], State.PENDING);
					return msg;
				}
				else {
					System.out.println(Lang.getTrans("errorPro"));
				}
				
				return null; 
			case "-r":
				if( State.INIT == UserList.getStateOfUser(parse[2])) {
					
					ByteBuffer atmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
					atmp.clear();
					atmp.putInt(CR.ReponseRequeteConvPrivee.ordinal());
					atmp.putInt(0);
					ByteBuffer aname = Client.UTF8_CHARSET.encode(parse[2]);
					int size = aname.limit();
					atmp.putInt(size);
					atmp.put(aname);
					msg.add(Message.buildMessage(CR.ReponseRequeteConvPrivee.ordinal(), atmp, ConnectionType.PUBLIC, -1));
					UserList.changeUserState(parse[2], State.REFUSED);
					return msg;
				}
				else {
					System.out.println(Lang.getTrans("errorPro"));
				}
				
				return null; 
			case "-f":
				if( parse.length != 4 ) {
					return null;
				}
				
				UserList.sendQueryForFile(parse[2], parse[3]);
			
				return null;
			case "-fa":
				
				if( parse.length != 4) {
					return null;
				}
				
				UserList.sendResponseForFile(parse[2],parse[3], true);
				
				return null;
			case "-fr":
				if( parse.length != 4) {
					return null;
				}
				
				UserList.sendResponseForFile(parse[2],parse[3], false);
				
				return null;
			case "-fc":
				if( parse.length != 3) {
					return null;
				}
				UserList.clearFiles(parse[2]);
				return null;
			case "-fs":
				if( parse.length != 3) {
					return null;
				}
				UserList.stateFile(parse[2]);
				return null;
			default:
				return null;
		}
		
		
		
	}

	/**
	 * List all room
	 * @param input
	 * @return List of message
	 */
	public static List<Message> roomAnalyse(String input) {
		
		String[] parse = input.split(" ");
		List<Message> msg = new LinkedList<>();
		
		if( parse.length == 1) {
			msg.add(Commande.listOfRoom());
		}
		else if( parse.length == 2) {
			
			Connect.connectRoom(Integer.valueOf(parse[1]));
			msg.add(Commande.connectToRoom(parse[1]));
			
		}
		
		

		return msg;
	}

}
