package app.client.client;

import app.client.langue.Lang;
import app.client.thread.ScannerThread;
import app.client.unique.Connect;

import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * The CLIENT
 * @author bbuyuk
 *
 */
public class Client {	
	

	public static final Charset UTF8_CHARSET = Charset.forName("UTF8");
	public static final int BUFFER_SIZE = 1024;
	public static final int lengthName = 32;
	public static int DEFAULT_PORT_PRIVATE = 8888 ;
	
	public static void main(String[] args) {
		
			Scanner scan = new Scanner(System.in);
			String name = null;
		
			System.out.println("Private port: ");
			DEFAULT_PORT_PRIVATE =Integer.valueOf(scan.nextLine());
			// start client 
			System.out.println("Choose language: ");
			Lang.printLang();
			System.out.println("Your choice: ");
			Lang.setLang(Integer.valueOf(scan.nextLine()));
		
			
			// continue with a welcome
			System.out.println(Lang.getTrans("welcome"));
			System.out.println(Lang.getTrans("askName")+ "MAX "+ lengthName);
			name = scan.nextLine();
			
			if( name == null) {
				System.out.println(Lang.getTrans("fatalError"+": Error 1"));
				scan.close();
				return;
			}
			else if ( name.length() > lengthName ) {
				
				System.out.println(Lang.getTrans("fatalErrorName")+" ;"+"Error 2");
				scan.close();
				return;
			}
			
			Connect.setName(name);
			Connect.setScanner(scan);
			
			System.out.println(Lang.getTrans("registerName")+name);
			System.out.println(Lang.getTrans("leave"));
			
			String user = null;
			while(true) {
				System.out.println(Lang.getTrans("connect"));
				user = scan.nextLine();
					
				if( user.equals("/quit")) {
					break;
				}
				
				if(!Connect.setConnection(user)) {
					//error wrong information
					System.out.println(Lang.getTrans("errorConn"));
					continue;
				}
				
				//connection is set open it 
				// and run thread wait it 
				Connect.openConnection();
				ScannerThread ct = ScannerThread.createScanner(scan);

				if( ct == null) {
					System.out.println(Lang.getTrans("errorScan"));
					continue;
				}

				Thread mainT = new Thread(ct);
				mainT.start();

				// wait to finish 
				try {
					mainT.join();
				} catch (InterruptedException e) {
					// do nothing 
					// continue
					scan.close();
				}
				
				

			}
		
			// close scan
			scan.close();
			System.out.println(Lang.getTrans("quit"));
			System.exit(0);
		
	}
	
}
