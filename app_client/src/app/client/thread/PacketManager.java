package app.client.thread;


import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import app.client.client.Client;
import app.client.langue.Lang;
import app.client.rfc.CR;
import app.client.rsc.Message;
import app.client.test.Debug;
import app.client.unique.Connect;
import app.client.unique.UserList;

/**
 * PacketManager , a thread that manage your packet
 * Give him a queue with message, filled by a reader thread or other
 * analyse message and print it 
 * launch a second little thread to manage multiple message
 * @author bbuyuk
 *
 */
/**
 * @author bali
 *this is the packetmanager it handle all of the received packet
 */
public class PacketManager implements Runnable {
	
	/**
	 * classique message
	 */
	private BlockingQueue<Message> msgToRead ;
	/**
	 * multiple message 
	 */
	private BlockingQueue<Message> trendingMsg = new LinkedBlockingQueue<>();
	
	private BlockingQueue<Message> fileMsg = new LinkedBlockingQueue<>();
	
	private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	/*yyyy/MM/dd HH:mm:ss*/
	private Date date = new Date();
	
	private Thread multipleMessage = new Thread(MultipleMessage.setMM(trendingMsg));
	private Thread fileMessage = new Thread(FileManager.setFM(fileMsg));

	private PacketManager(BlockingQueue<Message> msg) {
		this.msgToRead = msg;
	}
	
	
	/**
	 * Get a  packet manager
	 * @param msg blocking queue for your packet manager , this a queue for message to read
	 * @return PacketManager 
	 */
	public static PacketManager createManager(BlockingQueue<Message> msg) {
		
		if(msg == null) {
			return null;
		}
		
		return new PacketManager(msg);
	}
	
	@Override
	public void run() {


		Message msg = null;
		multipleMessage.start();
		fileMessage.start();
		while(!Thread.interrupted()) {


			if( Connect.isClosed()) {
				multipleMessage.interrupt();
				fileMessage.interrupt();
				return;
			}

			//public part 
				try {
					msg = msgToRead.take();
				} catch (InterruptedException e1) {
					multipleMessage.interrupt();
					fileMessage.interrupt();
					return;
				}
				
				
				// unique message
				if( msg.getCR() == CR.ReceptionMessageUnique.ordinal()) {
					Debug.printDebug("PM UNIQUE MESSAGE");
					System.out.println(Lang.getTrans("received")+ this.dateFormat.format(date)+ " " +Client.UTF8_CHARSET.decode(msg.getBuffer().flip()));

				}
				else if (msg.getCR() == CR.EnvoieMessageUnique.ordinal()) {
					Debug.printDebug("PM UNIQUE MESSAGE PRIV");
					System.out.println(Lang.getTrans("received")+ this.dateFormat.format(date)+ " " + msg.getInfo() + ":priv:"+Client.UTF8_CHARSET.decode(msg.getBuffer().flip()));
					
				}
				else if ( msg.getCR() == CR.EnvoieMessageMultiple.ordinal() ||
						  msg.getCR() == CR.FinMessageMultiple.ordinal()
						) {
					Debug.printDebug("PM MULTIPLE MESSAGE PRIV");
					this.trendingMsg.add(msg);
					
				}
				else if( msg.getCR() == CR.ReceptionMessageMultiple.ordinal() ||
						 msg.getCR() == CR.FinReceptionMessageMultiple.ordinal()
						
						){
					Debug.printDebug("PM MULTIPLE OR END");
					//multiple message
					this.trendingMsg.add(msg);
					
				}
				else if(msg.getCR() == CR.ReponseRequeteConvPrivee.ordinal()) {
					Debug.printDebug("PM REQUETE REP CONV PRIVEE");
					UserList.answearFrom(msg);
					
				}
				else if( msg.getCR() == CR.InformationConvPrivee.ordinal()) {
					Debug.printDebug("PM REQUETE INF CONV PRIVEE");				
					UserList.startUser(msg,msgToRead);
				}
				else if ( msg.getCR() == CR.DemandeConversationPriveeServeur.ordinal()) {
					Debug.printDebug("PM REQUETE DEMANDE REQUETE PRIVE 20");
					UserList.initUser(msg);
					
				}
				else if( msg.getCR() == CR.ListeSalle.ordinal()) {
					
					Debug.printDebug("PM REQUETE DEMANDE REQUETE LISTE SALLE");				
					ByteBuffer tmp = msg.getBuffer();
					tmp.flip();
					System.out.println("Tu es dans la salle: " + Connect.inRoomNb());
					for(int i=0; i < (tmp.limit()/Integer.BYTES/3) ; i++) {
						System.out.println("Salle N: "+ tmp.getInt() + " Dispo: " + tmp.getInt() + "/" + tmp.getInt());
					}
					
				}
				else if ( msg.getCR() == CR.ReponseSalle.ordinal()) {
					Debug.printDebug("PM REQUETE DEMANDE REQUETE LISTE SALLE");				
					
					int rep = msg.getBuffer().flip().getInt();
					
					if( rep == 0) {
						System.out.println("Vous avez changé de salle");
					}
					else {
						System.out.println("Erreur ... changement salle");
					}
					
					
				}
				else if ( msg.getCR() == CR.DemandeEnvoieFichier.ordinal()) {
					
					Debug.printDebug("PM REQUETE DEMANDE DE FICHIER");									
					ByteBuffer fname = msg.getBuffer();
					
					String fn = Client.UTF8_CHARSET.decode(fname.flip()).toString();
					
					System.out.println("(priv query): "+ msg.getInfo()+ " want to send this file to you: " + fn);
					
					UserList.receiveQueryForFile(msg.getInfo(), fn);

					
				}
				else if( msg.getCR() == CR.RequeteFichier.ordinal()) {
					
					Debug.printDebug("PM REQUETE RECPETIN FICHIER");									
					ByteBuffer rname = msg.getBuffer().flip();
					
					int value = rname.getInt();
					String frname =  Client.UTF8_CHARSET.decode(rname).toString();
					
					UserList.responseForFile(msg.getInfo(), frname, value==0?false:true);
					
				}
				else if ( msg.getCR() == CR.ReceptionFichier.ordinal() || 
						CR.FinFichier.ordinal() == msg.getCR()
						) {
					
					
					this.fileMsg.add(msg);
					
				}
				else {
					
					Debug.printDebug("CR NOT recognized error gonna happeen now" + msg.getCR());
					
					
				}

			

		}			





	}
	
	
	

}
