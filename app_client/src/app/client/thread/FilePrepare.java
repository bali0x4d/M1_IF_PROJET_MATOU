package app.client.thread;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import app.client.client.Client;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.server.UserContext;
import app.client.test.Debug;

/**
 * @author bali
 *REad a file , prepare it and fill the blocking uqeuue of user 
 */
public class FilePrepare implements Runnable {
	
	private String fileAll;
	private String fileName;
	private UserContext usr;
	
	public FilePrepare(String fileAll, String fileName , UserContext usr) {
		
		this.fileAll = fileAll;
		this.fileName = fileName;
		this.usr = usr;
		
	}
	

	@Override
	public void run() {
		
		RandomAccessFile aFile = null;
		try {
			aFile = new RandomAccessFile(fileAll, "r");
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			usr.removeFiles();
			usr.sendResponseForFiles(fileName, false);
			return;
		}
		FileChannel inChannel = aFile.getChannel();
		ByteBuffer buffer = ByteBuffer.allocate(Client.BUFFER_SIZE-Integer.BYTES);
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		try {
			while(inChannel.read(buffer) > 0)
			{
				
				tmp.clear();

				
				if( buffer.position() == (Client.BUFFER_SIZE-Integer.BYTES)) {
					Debug.printDebug(CR.ReceptionFichier.ordinal() + " in ");
					tmp.putInt(CR.ReceptionFichier.ordinal());
					buffer.flip();
					tmp.put(buffer);
					usr.putMessageFiles(Message.buildMessage(CR.ReceptionFichier.ordinal(), tmp, ConnectionType.PRIVATE, usr.getToken()));
					
				}else {
					Debug.printDebug(CR.ReceptionFichier.ordinal() + " out ");								
					tmp.putInt(CR.FinFichier.ordinal());
					buffer.flip();
					tmp.putInt(buffer.limit());
					tmp.put(buffer);
					usr.putMessageFiles(Message.buildMessage(CR.FinFichier.ordinal(), tmp, ConnectionType.PRIVATE, usr.getToken()));
					break;
					
					
				}
					
				buffer.clear(); // do something with the data and clear/compact it.
			}
		} catch (IOException e1) {
			//do nothing
			usr.removeFiles();
		}
		
		try {
			inChannel.close();
		} catch (IOException e) {
			//do nothing
			usr.removeFiles();
		}
		
		try {
			aFile.close();
		} catch (IOException e) {
			//do nothing
			usr.removeFiles();
		}
		
		usr.removeFiles();
		
	}

}
