package app.client.thread;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.BlockingQueue;

import app.client.client.Client;
import app.client.rsc.Message;
import app.client.test.Debug;
import app.client.unique.Connect;

/**
 * Perform a write in socketChannel 
 * This thread wait to take a message in the blocking queue and send it if a valid connect classe is not closed
 * 
 * @author bbuyuk
 *
 */
public class Write implements Runnable {
	
	private ByteBuffer bbout = ByteBuffer.allocateDirect(Client.BUFFER_SIZE);
	private BlockingQueue<Message> msgToSend = null;
	private SocketChannel sc = Connect.getSC(); // automatic connection

	
	private Write(BlockingQueue<Message> bq) {
		
		this.msgToSend = bq;
		
	}
	
	/**
	 * get a write ready 
	 * @param bq Blocking Queue with Message object in 
	 * @return Write Thread ready to start
	 */
	public static Write setWrite(BlockingQueue<Message> bq) {
		
		if( bq == null) {
			return null;
		}
		
		return new Write(bq);
		
	}

	@Override
	public void run() {
		
		int write = 0;
		Message msg = null;
		while(!Thread.interrupted()) {
			
			bbout.clear();
			
			
			try {
				msg = msgToSend.take();
			} catch (InterruptedException e1) {
				Debug.printDebug("WRITE CLOSED 1");
				return;
			}
			
			bbout.put(msg.getBuffer().flip());
			bbout.flip();			
			
			if( Connect.isClosed()) {
				Debug.printDebug("WRITE CLOSED 2");
				return;
			}
			
			Debug.printDebug("SENDING " + msg.getCR());
			while( bbout.hasRemaining() && !Connect.isClosed()) {
				try {
					write = sc.write(bbout);
				} catch (IOException e) {
					// do nothing
				}
			}
			Debug.printDebug("SENDED");
			
			if( write == -1) {
				Debug.printDebug("READ CLOSED 3");
				Connect.closedByServer();
				return;
			}
		}
		

	}

}
