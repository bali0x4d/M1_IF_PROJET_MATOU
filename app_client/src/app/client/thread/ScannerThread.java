package app.client.thread;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import app.client.client.Client;
import app.client.commande.Commande;
import app.client.commande.CommandeType;
import app.client.langue.Lang;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.server.Server;
import app.client.test.Debug;
import app.client.unique.Connect;
import app.client.user.MessageUtils;

/**
 * ScannerThread, 
 * simple class to perform input from user
 * set the connection , manage other threads 
 * @author bbuyuk
 *
 */
public class ScannerThread implements Runnable{
	
	
	private Scanner scan = null;
	
	// prepare tcp in 
	// prepare tcp out
	Thread in;
	Thread out;
	Thread printer;
	Thread server;

	
	
	//Queue message en attente public target
	private BlockingQueue<Message> msgToSend = new LinkedBlockingQueue<>();
	private BlockingQueue<Message> msgToRead = new LinkedBlockingQueue<>();
	

	
	
	private ScannerThread(Scanner in) {
		this.scan = in;
	}
		
	

	/**
	 * @param in Input from user , have to be a scanner
	 * @return ScannerThread get a scanner
	 */
	public static ScannerThread createScanner(Scanner in) {
		
		if( in == null) {
			// error
			return null;
		}
		
		return new ScannerThread(in);
		
	}
	
	@Override
	public void run() {

        //ask connection ?
		if( !Connect.askConnection()) {
			System.out.println(Lang.getTrans("connFalse"));
			return;
		}


		// start blabla
		
		String userInput = null;

		
		this.startThread();

		while(!Thread.interrupted()) {
			
			if( Connect.isClosed()) {
				// closed pipe kill everybody ...
				this.endThread();
				return;
			}
			
			userInput = this.scan.nextLine();
			
			if( Commande.getCmdId(userInput) == null) {
				// simple message 
				
				List<Message> msg = MessageUtils.createMessage(userInput, ConnectionType.PUBLIC, -1);
				
				if( msg != null) {

					msg.forEach(m -> msgToSend.add(m));
					
				}
			}
			else if( Commande.getCmdId(userInput) == CommandeType.quit.ordinal()) {				
				// leave ...
				// Send interrupt to server
				msgToSend.add(Commande.sendQuit(Connect.getToken()));
				
				// wait before leave...
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					
				}
				finally {
					Connect.closedByServer();
					this.endThread();
				}
				return;
			}
			else if( Commande.getCmdId(userInput) == CommandeType.name.ordinal()) {
				System.out.println("ID :"+Connect.getName());
			}
			else if( Commande.getCmdId(userInput) == CommandeType.help.ordinal() ) {
				System.out.println(Commande.getHelp());
				
			}
			else if( Commande.getCmdId(userInput) == CommandeType.user.ordinal()) {
				
				//send user request
				msgToSend.add(Commande.userList(-1));
			}
			else if( Commande.getCmdId(userInput) == CommandeType.room.ordinal()) {
				
				// simple message 
				List<Message> msg = Commande.roomAnalyse(userInput);
				
				if( msg != null) {

					msg.forEach(m -> msgToSend.add(m));
					
				}				
				
			}
			else if( Commande.getCmdId(userInput) == CommandeType.priv.ordinal()) {
				
				
				// simple message 
				List<Message> msg = Commande.privAnalyse(userInput);
				
				if( msg != null) {

					msg.forEach(m -> msgToSend.add(m));
					
				}
				
			}
			else if ( Commande.getCmdId(userInput) == CommandeType.luser.ordinal()) {
				
				Message msg = Commande.userList(-1);
				
				msgToSend.add(msg);
				
			}
			else {
				
				System.out.println("ERROR , maybe you're not connected to a room");
			}

		}

}
	
	
	private void startThread() {

		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.clear();
		tmp.putInt(CR.PortPrivee.ordinal());
		tmp.putInt(Client.DEFAULT_PORT_PRIVATE);
		
		this.msgToSend.add(Message.buildMessage(CR.PortPrivee.ordinal(), tmp, ConnectionType.PUBLIC, -1));
		

		 in = new Thread(Read.setRead(msgToRead));
		 out = new Thread(Write.setWrite(msgToSend));
		 printer = new Thread(PacketManager.createManager(msgToRead));
		 try {
			server = new Thread(Server.createServer(Client.DEFAULT_PORT_PRIVATE));
		} catch (IOException e) {
			Debug.printDebug("InternalServer for private failed");
		}
		
		in.start();
		out.start();
		printer.start();
		server.start();
		
	}
	
	private void endThread() {
		
		System.out.println(Lang.getTrans("errorDeco"));
		in.interrupt();
		out.interrupt();
		printer.interrupt();
		server.interrupt();
		
	}

}
