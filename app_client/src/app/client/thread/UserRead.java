package app.client.thread;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.BlockingQueue;

import app.client.client.Client;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.server.State;
import app.client.test.Debug;
import app.client.unique.UserList;

/**
 * @author bali
 *
 *Thisis the same of Read class but a little differents
 */
public class UserRead implements Runnable {

	
	final private SocketChannel sc;
	final private BlockingQueue<Message> msg;
	private boolean closed = false;
	private int token = 0;
	private String name = null;
	
	private UserRead(SocketChannel sc, BlockingQueue<Message> msg, String nm, int tk) {
		this.sc = sc;
		this.msg = msg;
		this.name = nm;
		this.token = tk;
	}
	
	/**
	 * get a new UserRead
	 * @param sc  socketchannel for read
	 * @param msg message queue for packetmamanger
	 * @return new UserRead
	 */
	public static UserRead createUserRead(SocketChannel sc, BlockingQueue<Message> msg, String nsrc, int tksrc,  String ndst, int tkdst) {
		// first send auth 
		// then close everybody
		
		try {
			sc.configureBlocking(false);
		} catch (IOException e1) {
		}
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		tmp.clear(); tmp.putInt(CR.AuthentificationPrivee.ordinal());
		tmp.putInt(tksrc);
		ByteBuffer namebf = Client.UTF8_CHARSET.encode(nsrc);
		tmp.putInt(namebf.limit());
		tmp.put(namebf);
	
		if(!writeFullyWithTimeOut(tmp,sc)) {
			Debug.printDebug("write");
			UserList.changeUserState(ndst, State.REFUSED);
			try {
				sc.configureBlocking(true);
			} catch (IOException e) {
			}
			return null;
		}
		try {
			sc.configureBlocking(true);
		} catch (IOException e) {
		}
		
		
		return new UserRead(sc,msg,ndst,tkdst);
	}
	
	@Override
	public void run() {

		ByteBuffer bbin = ByteBuffer.allocate(Client.BUFFER_SIZE);

		while(!Thread.interrupted()) {

			if(this.closed) {
				System.out.println("Votre correpondant à fermé son tunnel de lecture");
				return;				
			}

			// read request by request
			bbin.clear();
			bbin.limit(Integer.BYTES);
			if( !readFully(bbin)) {		
				// Errror close connection and get out
				this.closed = true;
				Debug.printDebug("READ CLOSED 2 USERREAD");
				System.out.println("Votre correpondant à fermé son tunnel de lecture");
				UserList.removeUser(name);
				return;
			}

			bbin.flip();
			int CR = bbin.getInt();
			Debug.printDebug("Received packet CR USERREAD => "+ CR);
			ConnectionType ct = ConnectionType.PRIVATE;
			Long token = Long.valueOf(this.token);
			ByteBuffer tmp = requestPriv(CR);


			if( tmp != null) {
				Message msgR = Message.buildMessage(CR, tmp, ct, token);
				msgR.setInfo(this.name);
				this.msg.add(msgR);
			}
		}


	}
		

	
	
	
	/**
	 * try to fill until there no remaining / have to be no blocking mode
	 * @param bf ByteBuffer
	 * @return true if success , false if not
	 */
	private static boolean writeFullyWithTimeOut(ByteBuffer bf, SocketChannel sc) {
		
		int write = 0;
		long timeOut = 20000;		
		long time = System.currentTimeMillis();
		bf.flip();
		
			while(write != -1) {
				
				try {
					write = sc.write(bf);
				} catch (IOException e) {
					return false;
				}
					
				
				
				if( !bf.hasRemaining()) {
					return true;
				}
				
				if( (System.currentTimeMillis() - time) > timeOut) {
					return false;
				}
				
				
			}
			return false;

	}
	
	/**
	 * try to fill until there no remaining
	 * @param bf ByteBuffer
	 * @return true if success , false if not
	 */
	public boolean readFully(ByteBuffer bf) {
		
		int read = 0;
			
			while(read != -1) {
				
				try {
					read = sc.read(bf);
				} catch (IOException e) {
					Debug.printDebug("ERR1 USER READ");
					this.closed = true;
					return false;
				}
					
				if( !bf.hasRemaining()) {
					return true;
				}
				
				
			}
			
			Debug.printDebug("ERR2 USER READ");
			this.closed = true;
			// connection closed
			return false;
			

		
		
	}
	
	
	private ByteBuffer requestPriv(Integer cr) {
		
		if( cr == CR.EnvoieMessageUnique.ordinal()) {
			//message unique recu
			return uniqueMessage();
			
		}
		else if( cr == CR.EnvoieMessageMultiple.ordinal() || cr == CR.FinMessageMultiple.ordinal()) {
			//message multiple
			return multipleMessage(cr);
			
		}
		else if( cr == CR.DemandeEnvoieFichier.ordinal()) {
			
			return demandeFichier(cr);
			
		}
		else if( cr == CR.RequeteFichier.ordinal()) {
			
			// sapplique ici a la reponse fichier
			return requeteFichier(cr);
			
		}
		else if ( cr == CR.ReceptionFichier.ordinal() || cr == CR.FinFichier.ordinal()) {
			
			// reception fichier
			return receptionFichier(cr);
			
		}
		else {
			System.out.println("CR non gere en prive , closing private ");
			UserList.removeUser(name);
		}
		
		
		
		return null;
		
	}
	
	
	private ByteBuffer receptionFichier(Integer cr) {
		ByteBuffer tmp = null;
		
		if( cr == CR.ReceptionFichier.ordinal()) {
			
			
			tmp = ByteBuffer.allocate(Client.BUFFER_SIZE-Integer.BYTES);
			tmp.clear();
			if( !readFully(tmp)) {
				System.out.println("ERROR 1 READ FICHIER... close connection");
				UserList.removeUser(this.name);
				return null;
			}				
			
			
			
			return tmp;
			
		}
		else {
			
			tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
			
			tmp.clear();
			tmp.limit(Integer.BYTES);
			
			if( !readFully(tmp)) {
				System.out.println("ERROR 2 READ FICHIER... close connection");
				UserList.removeUser(this.name);
				return null;
			}				
			
			int size = tmp.flip().getInt();
			
			tmp.clear();
			tmp.limit(size);
			
			if( !readFully(tmp)) {
				System.out.println("ERROR 3 READ FICHIER... close connection");
				UserList.removeUser(this.name);
				return null;
			}		
			
			return tmp;
			
		}
		

	}

	private ByteBuffer requeteFichier(Integer cr) {

		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.clear();
		tmp.limit(Integer.BYTES);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR 1 READ UNIQUE REQUETE FICHIER... close connection");
			UserList.removeUser(this.name);
			return null;
		}		
	
		int value = tmp.flip().getInt();
		
		tmp.clear();
		tmp.limit(Integer.BYTES);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR 2 READ UNIQUE REQUETE FICHIER... close connection");
			UserList.removeUser(this.name);
			return null;
		}			
		int size = tmp.flip().getInt();
		
		
		tmp.clear();
		tmp.limit(size);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR 3 READ UNIQUE REQUETE FICHIER... close connection");
			UserList.removeUser(this.name);
			return null;
		}		
		
		ByteBuffer stmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		stmp.putInt(value);
		stmp.put(tmp.flip());
		
		
		return stmp;
		
	}

	private ByteBuffer demandeFichier(Integer cr) {
		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.clear();
		tmp.limit(Integer.BYTES);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR 1 READ UNIQUE FICHIER DEMANDE... close connection");
			UserList.removeUser(this.name);
			return null;
		}
		
		
		int size = tmp.flip().getInt();
		
		tmp.clear();
		tmp.limit(size);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR 2 READ UNIQUE FICHIER DEMANDE... close connection");
			UserList.removeUser(this.name);
			return null;
		}		
		
		return tmp;
		
	}

	private ByteBuffer multipleMessage(Integer cr) {
		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		if( cr == CR.EnvoieMessageMultiple.ordinal()) {
			
			
			tmp.clear();
			tmp.limit(Integer.BYTES);
			
			if( !readFully(tmp)) {
				System.out.println("ERROR READ UNIQUE MESSAGE MULTIPLE 1... close connection");
				UserList.removeUser(this.name);
				return null;
			}
			
			tmp.clear();
			tmp.limit(Client.BUFFER_SIZE- (2*Integer.BYTES));
			
			if( !readFully(tmp)) {
				System.out.println("ERROR READ UNIQUE MESSAGE MULTIPLE 2... close connection");
				UserList.removeUser(this.name);
				return null;
			}			
			
			return tmp;
		}
		else {
			
			
			tmp.clear();
			tmp.limit(Integer.BYTES);
			
			if( !readFully(tmp)) {
				System.out.println("ERROR READ UNIQUE MESSAGE MULTIPLE 1... close connection");
				UserList.removeUser(this.name);
				return null;
			}
			
			
			tmp.clear();
			tmp.limit(Integer.BYTES);
			
			if( !readFully(tmp)) {
				System.out.println("ERROR READ UNIQUE MESSAGE MULTIPLE 1... close connection");
				UserList.removeUser(this.name);
				return null;
			}			
			
			int size = tmp.flip().getInt();
			
			tmp.clear();
			tmp.limit(size);
			
			if( !readFully(tmp)) {
				System.out.println("ERROR READ UNIQUE MESSAGE MULTIPLE 2... close connection");
				UserList.removeUser(this.name);
				return null;
			}			
			
			return tmp;			
			
			
			
		}
		
	}

	private ByteBuffer uniqueMessage() {
		
		
		//message unique recu
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.clear();
		tmp.limit(Integer.BYTES);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR READ UNIQUE MESSAGE 1... close connection");
			UserList.removeUser(this.name);
			return null;
		}
		
		@SuppressWarnings("unused")
		int token = tmp.flip().getInt();
		
		
		tmp.clear();
		tmp.limit(Integer.BYTES);
		

		if( !readFully(tmp)) {
			System.out.println("ERROR READ UNIQUE MESSAGE 1... close connection");
			UserList.removeUser(this.name);
			return null;
		}
		
		int size = tmp.flip().getInt();
		tmp.clear();
		tmp.limit(size);
		
		if( !readFully(tmp)) {
			System.out.println("ERROR READ UNIQUE MESSAGE 2 ... close connection");
			UserList.removeUser(this.name);
			return null;
		}
		
		return tmp;
	}

}
