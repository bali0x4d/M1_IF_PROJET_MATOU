package app.client.thread;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.BlockingQueue;

import app.client.rsc.Message;
import app.client.server.UserContext;
import app.client.server.UserContext.FilesOP;
import app.client.unique.UserList;

/**
 * @author bali
 *fileManager for file
 */
public class FileManager implements Runnable {

	private BlockingQueue<Message> queue = null;
	
	
	
	private FileManager(BlockingQueue<Message> fileMsg) {
		this.queue = fileMsg;
	}

	@SuppressWarnings("resource")
	@Override
	public void run() {

		Message msg = null;
		UserContext usr = null;
		FilesOP fl = null;
		File file = null;
		FileChannel fileC = null;
		ByteBuffer bf = null;

		while(!Thread.interrupted()) {
			
			try {
				msg = queue.take();
			} catch (InterruptedException e) {
				return;
			}
			
			usr = UserList.getUser(msg.getInfo());
			fl = usr.getFiles();
			if( usr == null || fl == null) {
				continue;
			}

			
			file = new File(fl.clearedName);
			
			if( !file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e) {
					continue;
				}
			}
			
			try {
				fileC = new FileOutputStream(file,true).getChannel();
			} catch (FileNotFoundException e) {
				continue;
			}
			
			bf = msg.getBuffer().flip();
			try {
				fileC.write(bf);
			} catch (IOException e) {
				continue;
			}
			
			try {
				fileC.close();
			} catch (IOException e) {
				continue;
			}
			
			
			

		}

	


}

	/**
	 * Set up a fileManager
	 * @param fileMsg a blocking Queue
	 * @return FileManager
	 */
	public static FileManager setFM(BlockingQueue<Message> fileMsg) {

		return new FileManager(fileMsg);
	}

}
