package app.client.thread;

import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;

import app.client.client.Client;
import app.client.langue.Lang;
import app.client.rfc.CR;
import app.client.rsc.Message;
import app.client.user.MessageUtils;

/**
 * @author bali
 *
 *Multiplemessage thread 
 */
public class MultipleMessage implements Runnable {

	private BlockingQueue<Message> trendingMsg;
	
	private HashMap<Long,ByteBuffer> message = new HashMap<>();
	
	private DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	/*yyyy/MM/dd HH:mm:ss*/
	private Date date = new Date();

	private MultipleMessage(BlockingQueue<Message> msg) {
		this.trendingMsg = msg;
	}
	
	public static MultipleMessage setMM(BlockingQueue<Message> msg) {
		
		if( msg == null) {
			return null;
		}
		
		return new MultipleMessage(msg);
	}
	
	@Override
	public void run() {
		
		Message msg = null;
		while(!Thread.interrupted()) {
			
			
			
			try {
				msg = trendingMsg.take();
			} catch (InterruptedException e) {
				return;
			}
			
			message.computeIfAbsent(msg.getFrom(), m-> {
				return ByteBuffer.allocateDirect(Client.BUFFER_SIZE*MessageUtils.LimitMessage);
			});
			
			if( msg.getCR() == CR.ReceptionMessageMultiple.ordinal() || msg.getCR() == CR.EnvoieMessageMultiple.ordinal()) {
				
				ByteBuffer tmp = this.message.get(msg.getFrom());
				
				if( tmp.remaining() >= Client.BUFFER_SIZE ) {
					
					tmp.put(msg.getBuffer().flip());
					
				}
				else {
					
					// error flush it
					tmp.clear();
					// and remove it ...
					this.message.remove(msg.getFrom());
					
					
				}
				
			}
			else if(msg.getCR() == CR.FinReceptionMessageMultiple.ordinal() ) {
				
				ByteBuffer tmp = this.message.get(msg.getFrom());
				
				if( tmp.remaining() >= Client.BUFFER_SIZE ) {
					
					tmp.put(msg.getBuffer().flip());
					
					tmp.flip();
					
					System.out.println(Lang.getTrans("received")+ this.dateFormat.format(date)+ " " +Client.UTF8_CHARSET.decode(tmp));
					
					tmp.clear();
					// then remove 
					this.message.remove(msg.getFrom());

					
				}
				else {
					
					// error flush it
					tmp.clear();
					// and remove it ...
					this.message.remove(msg.getFrom());
					
					
				}				
				
				
				
			}
			else if(msg.getCR() == CR.FinMessageMultiple.ordinal()) {
				
				ByteBuffer tmp = this.message.get(msg.getFrom());
				
				if( tmp.remaining() >= Client.BUFFER_SIZE ) {
					
					tmp.put(msg.getBuffer().flip());
					
					tmp.flip();
					
					System.out.println(Lang.getTrans("received")+ this.dateFormat.format(date)+ " " + msg.getInfo() + ":priv:" + Client.UTF8_CHARSET.decode(tmp));
					
					tmp.clear();
					// then remove 
					this.message.remove(msg.getFrom());

					
				}
				else {
					
					// error flush it
					tmp.clear();
					// and remove it ...
					this.message.remove(msg.getFrom());
					
					
				}					
			}
			else {
				//do nothing	
			}
			
			
			
		}

	}

}
