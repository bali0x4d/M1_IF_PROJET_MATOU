
package app.client.thread;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.BlockingQueue;

import app.client.client.Client;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.rsc.RequestRead;
import app.client.test.Debug;
import app.client.unique.Connect;

/**
 * Read method from Socketchannel
 * Perform a read a put message in a queue
 * @author bbuyuk
 *
 */
/**
 * @author bbuyuk
 *
 */
public class Read implements Runnable {
	
	private ByteBuffer bbin = ByteBuffer.allocateDirect(Client.BUFFER_SIZE);
	private BlockingQueue<Message> msgToRead ;
	private SocketChannel sc = Connect.getSC();

	private Read(BlockingQueue<Message> bq) {
		
		this.msgToRead = bq;
		
	}
	
	/**
	 * @param bq blockingqueue to put message
	 * @return Read
	 */
	public static Read setRead(BlockingQueue<Message> bq) {
		
		if( bq == null) {
			return null;
		}
		
		return new Read(bq);
		
	}
	
	

	@Override
	public void run() {
		
		
		while(!Thread.interrupted()) {
			
			if( Connect.isClosed()) {
				Debug.printDebug("READ CLOSED 1");
				return;
			}
			
			
			// read request by request
			bbin.clear();
			bbin.limit(Integer.BYTES);
			if( !readFully(bbin)) {		
				// Errror close connection and get out
				Connect.closedByServer();
				Debug.printDebug("READ CLOSED 2");
				return;
			}
			
			bbin.flip();
			int CR = bbin.getInt();
			Debug.printDebug("Received packet CR => "+ CR);
			ConnectionType ct = null;
			Long token = Long.valueOf(0);
			ByteBuffer tmp = RequestRead.request(CR,ct, token);
			
			if( tmp != null) {
				Message msg = Message.buildMessage(CR, tmp, ct, token);
				msgToRead.add(msg);
			}
		}
	}
	
	
	/**
	 * try to fill until there no remaining
	 * @param bf ByteBuffer
	 * @return true if success , false if not
	 */
	public boolean readFully(ByteBuffer bf) {
		
		int read = 0;
			
			while(read != -1) {
				
				try {
					read = sc.read(bf);
				} catch (IOException e) {
					Debug.printDebug("ERR1");
					Connect.closedByServer();
					return false;
				}
					
				if( !bf.hasRemaining()) {
					return true;
				}
				
				
			}
			
			Debug.printDebug("ERR2");
			// connection closed
			Connect.closedByServer();
			return false;
			

		
		
	}

}
