package app.client.user;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

import app.client.client.Client;
import app.client.langue.Lang;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.unique.Connect;

/**
 * 
 * MessageUtils simple classe 
 * create message unique or multiple 
 * @author bbuyuk
 *
 */
public class MessageUtils {
	
	public final static int LimitMessage = 30; // default LimitMessage * 
	
	
	/**
	 * Create a list of message from string 
	 * @param str a string that contains your message
	 * @return List of message
	 */
	public static List<Message> createMessage(String str, ConnectionType ct , long tk) {
		
		if( str == null || str.length() <= 0) {
			System.out.println(Lang.getTrans("errorMsgLen")  
					 + " :2");
			return null;
		}
		
		ByteBuffer msg = Client.UTF8_CHARSET.encode(str);
				
		long nbMsg = msg.limit()/Client.BUFFER_SIZE;
		
		if( (nbMsg+(nbMsg*(Integer.BYTES*3)/Client.BUFFER_SIZE)) > LimitMessage ) {
			System.out.println(Lang.getTrans("errorMsgLen")); 
			return null;
			
		}
		
		int cr = 0;
		
		if( msg.limit() <= (Client.BUFFER_SIZE-(Integer.BYTES*2)) ) {
			cr = CR.EnvoieMessageUnique.ordinal();
		}
		else {
			cr = CR.EnvoieMessageMultiple.ordinal();
		}
		List<Message> tmp = new LinkedList<>();
		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		while(msg.hasRemaining()) {
			bf.clear();
			bf.putInt(cr);
			bf.putInt(Connect.getToken());

			if( msg.remaining() > bf.remaining()) {
				int ol = msg.limit();
				msg.limit(msg.position() + bf.remaining());
				bf.put(msg);
				msg.limit(ol);
			}
			else {
				bf.putInt(msg.remaining());
				bf.put(msg);
			}
			tmp.add(Message.buildMessage(cr, bf, ct, tk));
		}
		if( cr == CR.EnvoieMessageMultiple.ordinal()) {
			//last become 
			tmp.get(tmp.size()-1).changeCR(CR.FinMessageMultiple.ordinal());;
			
		}
		return tmp;
	}
	


}
