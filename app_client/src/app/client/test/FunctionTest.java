package app.client.test;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;

import org.junit.jupiter.api.Test;

import app.client.client.Client;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.user.MessageUtils;

class FunctionTest {

	@Test
	void messageTestUnique() {
		
		Charset ch = Charset.forName("UTF8");
		
		List<Message> msg = MessageUtils.createMessage("essai", ConnectionType.PUBLIC, -1);
		
		ByteBuffer bf = msg.get(0).getBuffer().flip();
		bf.getInt();
		bf.getInt();
		bf.getInt();
		String tmp = ch.decode(bf).toString();

		
		assertEquals("essai",tmp);
		
		
		
	}
	
	
	
	@Test
	void messageTestMultiple() {
		
		Charset ch = Charset.forName("UTF8");
		
		String strLong = "\n" + 
				"\n" + 
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed diam lorem, egestas id sodales ut, feugiat sed libero. Cras tempor justo felis, vel ornare massa ultricies ut. Duis condimentum consectetur lorem, at semper dui gravida sit amet. Donec ultricies metus non dui dignissim tempus. Cras sit amet risus quis metus aliquet euismod nec ac dui. Aliquam maximus tincidunt massa, in vehicula turpis maximus non. Donec condimentum viverra urna ultricies mattis. Vivamus placerat, lacus id lobortis hendrerit, elit dui vestibulum neque, et feugiat neque velit et mi. Nulla cursus fringilla ipsum, eget viverra mi aliquet pellentesque. Aliquam erat volutpat. Vestibulum mattis urna neque, et suscipit lacus aliquam id. Quisque vitae sapien elit. Pellentesque vulputate libero arcu, quis rutrum mi condimentum a.\n" + 
				"\n" + 
				"Donec justo magna, pretium in mattis vel, posuere ac eros. Maecenas hendrerit metus vitae ornare iaculis. In congue volutpat nisl, hendrerit pharetra mi ornare at. Integer vehicula lectus at convallis cursus posuere. ";
		
		System.out.println(strLong.length());
		
		
		List<Message> msg = MessageUtils.createMessage(strLong, ConnectionType.PUBLIC, -1);
	
		ByteBuffer st = ByteBuffer.allocate(Client.BUFFER_SIZE*5);
				
		
		Message ms = null;
		for(int i=0; i < msg.size(); i++) {
			
			ms = msg.get(i);
			ByteBuffer tmp = ms.getBuffer();
			tmp.flip();
			if( ms.getCR() == CR.EnvoieMessageMultiple.ordinal()) {
				System.out.println(tmp.toString());
				System.out.println(tmp.getInt() + "/" + tmp.getInt());
				st.put(tmp);
			}
			else if ( ms.getCR() == CR.FinMessageMultiple.ordinal() ) {
				System.out.println(tmp.toString());
				System.out.println(tmp.getInt() + "/" + tmp.getInt() + "/" + tmp.getInt());				
				st.put(tmp);
			}
			
		}
		
		
		st.flip();
		String tmp = ch.decode(st).toString();

		
		assertEquals(strLong,tmp);
		
		
		
	}

}
