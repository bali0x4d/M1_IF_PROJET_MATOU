package app.client.server;


import app.client.client.Client;
import app.client.files.StateFiles;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.test.Debug;
import app.client.thread.FilePrepare;
import app.client.thread.UserRead;
import app.client.unique.Connect;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author bali
 *UserContext for server
 */
public class UserContext {
	
	static public class FilesOP{
		public String clearedName;
		public String fileName;
		public StateFiles st;
		
	}
	
	private int token;
	@SuppressWarnings("unused")
	private String address;
	@SuppressWarnings("unused")
	private int port;
	private String pseudo;

	/**
	 * sending file to this user 
	 */
	private FilesOP files = null;
	

	
	/**
	 * Thread to read from server
	 */
	private Thread reader;
	
	/**
	 * Thread to send files
	 */
	private Thread fileSender;
	
	/**
	 * InetSocketAddress
	 */
	private static InetSocketAddress isa = null;
	/**
	 * SocketChannel
	 */
	private static SocketChannel scin = null;
	/**
	 * closed , if read/write close , leave connection
	 */
	private static boolean closed = false;
	
	/**
	 *  state of connexion
	 */
	private State st;
	
	/**
	 * list of message to send ( used by server )
	 */
	final private BlockingQueue<Message> msgToSend = new LinkedBlockingQueue<>();
	
	/*
	 * message for file sending
	 * 
	 */
	private BlockingQueue<Message> msgToSendFiles = new LinkedBlockingQueue<>();
	
	
	private SelectionKey key;
	private SocketChannel scout;
	final private ByteBuffer bbout = ByteBuffer.allocate(Client.BUFFER_SIZE);
	private Server server;
	
	
	/**
	 * Get token of user
	 * @return int
	 */
	public int getToken() {
		return token;
	}
	
		
	private UserContext(String name) {
		
		this.pseudo = name;
		this.token = -1;
		this.st = State.INIT;
		
	}
	
	
	
    /**
     * close scin
     */
    private void silentlyCloseSCIN() {
       
    	if( scin != null) {
    		try {
    			scin.close();
    		} catch (IOException e) {
    			// ignore exception
    		}
    	}
    }
	
	
    /**
     * close scout
     */
    private void silentlyCloseSCOUT() {
       
    	if( this.scout != null) {
    		try {
    			scout.close();
    		} catch (IOException e) {
    			// ignore exception
    		}
    	}
    }
	
	/**
	 * @param server server info
	 * @param key key info 
	 */
	public void serverInfo(Server server, SelectionKey key) {
		this.server = server;
		this.key = key;
		
		this.scout = (SocketChannel) this.key.channel();
	}
	
	/**
	 * Set the name only 
	 * @param name the name of user
	 * @return simple user
	 */
	public static UserContext initUser(String name) {
		
		if( name == null || name.length() <= 0) {
			return null;
		}
		
		
		return new UserContext(name);
		
	}
	
	/**
	 * set connection for this user and connect it 
	 * @param name name of user
	 * @param tk token 
	 * @param address address 
	 * @param port port
	 * @param msgToRead trending queue for packetmanager
	 * @return boolean if success or not
	 */
	public boolean connectUser(String name, int tk,String address,int port, BlockingQueue<Message> msgToRead) {
		
		isa = new InetSocketAddress(address,port);
		
		if( !this.pseudo.equals(name)) {
			Debug.printDebug("Err1");
			return false;
		}
		
		if( isa.isUnresolved()) {
			Debug.printDebug("Err2");
			return false;
		}
		
		
		try {
			scin = SocketChannel.open();
			scin.connect(isa);
		} catch (IOException e) {
			Debug.printDebug("Err3");
			return false;
		}
		
		this.token = tk;
		this.address = address;
		this.port = port;
		this.st = State.WAITINGFORAUTH;
	    this.reader = new Thread(UserRead.createUserRead(scin, msgToRead, Connect.getName(), Connect.getToken(),this.pseudo,this.token));
		this.reader.start();
		return true;
		
	}
	
	
    /**
     * Add a message to bbout
     *
     */
	private void processOut() {

		Debug.printDebug("IN PROCESS OUT ");
		if( this.bbout.hasRemaining() && !this.msgToSend.isEmpty() ) {
			// fill it if size its okay
			Debug.printDebug("Sending message");

			if( !this.msgToSend.isEmpty()) {
				Debug.printDebug("AddingMessageToBuffer");
				Message msg = this.msgToSend.peek();
				ByteBuffer btmp = msg.getBuffer();
				Debug.printDebug("Added message cr "+ msg.getCR());

				if( bbout.remaining() >= btmp.flip().remaining()) {
					this.bbout.put(btmp);
					this.msgToSend.poll();
				}	
			}

		}
		else if(this.bbout.hasRemaining() &&  !this.msgToSendFiles.isEmpty() ) {

			Debug.printDebug("Sending files");
			if( !this.msgToSendFiles.isEmpty()) {
				Debug.printDebug("AddingMessageFilesToBuffer");
				Message msg = this.msgToSendFiles.peek();
				ByteBuffer btmp = msg.getBuffer();
				Debug.printDebug("Added message cr "+ msg.getCR());

				if( bbout.remaining() >= btmp.flip().remaining()) {
					this.bbout.put(btmp);
					this.msgToSend.poll();
				}	
			}

		}
		Debug.printDebug("OUT PROCESS IN");
	}
	
	
    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */

    private void updateInterestOps() {
    	int newIO = 0;
    	
    	Debug.printDebug("IN UPDATE OP");
    	// we want to write
    	if( this.bbout.position() > 0 || !this.msgToSend.isEmpty() || !this.msgToSendFiles.isEmpty()) {
    		Debug.printDebug("WRITE IT 1");
    		newIO |= SelectionKey.OP_WRITE;
    		
    	}

    	
    	if( closed) {
    		Debug.printDebug("WHAT its this");
    		this.closeConnection();
    	}
    	
    	if( newIO == 0) {
    		Debug.printDebug("DO NOTHING");
    		this.key.interestOps(newIO);
    	}
    	else {
    		Debug.printDebug("Change it");
    		this.server.wakeUp();
    		this.key.interestOps(newIO);
    	}
    }
	
    /**
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException
     */

    public void doWrite() throws IOException {
    	
    	Debug.printDebug("WRITING");
    	this.bbout.flip();    	    	
        this.scout.write(this.bbout);
     	this.bbout.compact();    	
    	this.processOut();    	
    	this.updateInterestOps();
    }	
	
	

    
	/**
	 * Send a message to user
	 * @param msg message to send
	 */
	public void putMessage(Message msg) {
		if(msg == null) {
			return;
		}
		Debug.printDebug("Message added in queue ready to send");
		this.msgToSend.add(msg);
		this.processOut();
		this.updateInterestOps();
	}
	
	/**
	 * Send a message ocntaining fils informations
	 * @param msg message to send
	 */
	public void putMessageFiles(Message msg) {
		if( msg == null) {
			return;
		}
		this.msgToSendFiles.add(msg);
		this.processOut();
		this.updateInterestOps();
	}
	
	/**
	 * close connection for user
	 */
	public void closeConnection() {
		
		if( this.reader != null && !this.reader.isInterrupted() ) {
			this.reader.interrupt();
		}
		
		if( this.fileSender != null && !this.fileSender.isInterrupted() ) {
			this.reader.interrupt();
		}		
		closed = true;
		this.st = State.CLOSED;
		

		this.silentlyCloseSCIN();
		this.silentlyCloseSCOUT();
		
	}


	/**
	 * Get state of user
	 * @return State or null
	 */
	public State getState() {
		return this.st;
	}
	
	
	/**
	 * Change the state of user
	 * @param st State
	 */
	public void changeState(State st) {
		
		this.st = st;
	}

	public FilesOP getFiles() {
		return this.files;
	}

	
	/**
	 * SendQuery for files
	 * @param fileName fileName
	 */
	public void qForFiles(String fileName) {
		
		File f = new File(fileName);
		String clearedName = f.getName();
		
		if( files != null ) {
			System.out.println("File already in process");
			return;
		}
		Debug.printDebug("Envoie de "+ clearedName);
		files= new FilesOP();
		files.st = StateFiles.ASKED;
		files.clearedName = clearedName;
		files.fileName = fileName;

		ByteBuffer msg = ByteBuffer.allocate(Client.BUFFER_SIZE);
		ByteBuffer file = Client.UTF8_CHARSET.encode(clearedName);
		
		msg.putInt(CR.DemandeEnvoieFichier.ordinal());
		
		
		if( msg.remaining() < (file.limit()+Integer.BYTES)) {
			System.out.println("Message name too long");
			files = null;
			return;
		}
		msg.putInt(file.limit());
		msg.put(file);
		
		this.putMessage(Message.buildMessage(CR.DemandeEnvoieFichier.ordinal(), msg, ConnectionType.PRIVATE, Connect.getToken()));
		
		
	}


	/**
	 * Received query for it 
	 * @param fileName fileName
	 */
	public void rForFiles(String fileName) {
		
		
		if( files != null ) {
			System.out.println("You're already in process");
			return;
		}
			files = new FilesOP();
			files.st = StateFiles.ASKED;
			files.clearedName = fileName;
			files.fileName = fileName;

		
		
	}


	/**
	 * Clean files part
	 */
	public void removeFiles() {
		Debug.printDebug("Clearing files");
		files = null;
	}
	
	
	/**
	 * @param fileName fileName
	 * @param value vlaue for repsonse
	 */
	public void rspForFiles(String fileName, boolean value) {
		
		
		if( files == null ) {
			System.out.println("Wrong name");
			return;
		}
		
		
		if( files.st != StateFiles.ASKED) {
			System.out.println("Already on road");
		}
		
		
		if( value == true) {
			
			files.st = StateFiles.SENDING;
			System.out.println("Sending files");
			if( files.st == StateFiles.SENDING) {
				this.fileSender = new Thread(new FilePrepare(files.fileName,files.clearedName,this));	
				this.fileSender.start();
			}
			
			
		}else {
			
			System.out.println("Files refused");
			files = null;
		}
		
	}


	/**
	 * @param fileName fileName 
	 * @param value true or false
	 */
	public void sendResponseForFiles(String fileName, boolean value) {
		
		if( value == false) {
			files = null;
		}
		else {
			files.st = StateFiles.SENDING;
			if( !fileName.equals(fileName)) {
				System.out.println("Wrong name");
				return;
			}
		}
		
		
		ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		tmp.putInt(CR.RequeteFichier.ordinal());
		tmp.putInt(value==true?1:0);
		
		ByteBuffer name = Client.UTF8_CHARSET.encode(fileName);
		
		tmp.putInt(name.limit());
		tmp.put(name);
		
		
		this.putMessage(Message.buildMessage(CR.RequeteFichier.ordinal(),tmp, ConnectionType.PRIVATE, this.token));
		
	
	}
	

	

}
