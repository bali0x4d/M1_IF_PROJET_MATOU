package app.client.server;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import app.client.client.Client;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.rsc.Message;
import app.client.test.Debug;
import app.client.unique.Connect;
import app.client.unique.UserList;
import app.client.user.MessageUtils;


/**
 * @author bali
 * InternalServer of user
 *
 */
public class Server implements Runnable {
	    
	@SuppressWarnings("unused")
	private final int port;
	
    private final ServerSocketChannel serverSocketChannel;
    private final Selector selector;
    private final Set<SelectionKey> selectedKeys;
       
    private final int timeOut = 30000;
    
    private Server(int port) throws IOException {
    	this.port = port;
    	serverSocketChannel = ServerSocketChannel.open();
    	serverSocketChannel.bind(new InetSocketAddress(port));
    	selector = Selector.open();
    	selectedKeys = selector.selectedKeys();
    }
    
    
    /**
     * @param port port for server
     * @return server...
     * @throws IOException IOEXCPETION
     */
    static public Server createServer(int port) throws IOException {
    	
    	if( port < 0 || port > 65336) {
    		throw new IllegalArgumentException();
    	}
    	
    	return new Server(port);
    	
    }
    
    
    
    
    
	@Override
	public void run() {
		
		try {
			serverSocketChannel.configureBlocking(false);
		} catch (IOException e1) {
			//do nothing
		}
		
		try {
			serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		} catch (ClosedChannelException e1) {
			//do nothing
		}
		
	    Set<SelectionKey> selectedKeyss = selector.selectedKeys();
	    
	    while (!Thread.interrupted()) {
	        printKeys();
	        System.out.println("Starting select");
	        try {
				selector.select();
			} catch (IOException e) {
				Debug.printDebug("IN ERROR WHY");
				return;
			}
	        System.out.println("Select finished");
	        printSelectedKey();
	        processSelectedKeys();
	        selectedKeyss.clear();
	    }

	}

	
	

	private void processSelectedKeys() {
		
        for (SelectionKey key : selectedKeys) {
            if (key.isValid() && key.isAcceptable()) {
                doAccept(key);
            }
            try {
                if (key.isValid() && key.isWritable()) {
                    ((UserContext) key.attachment()).doWrite();
                }
            } catch (IOException e) {
                ((UserContext) key.attachment()).closeConnection();
            }
        }
		
	}


	public void wakeUp() {
		this.selector.wakeup();
	}
	
	
	private void doAccept(SelectionKey key) {
		
    	SocketChannel sc;
		try {
			sc = this.serverSocketChannel.accept();
		} catch (IOException e2) {
			return;
		}
        
        if( sc == null) {
        	// erreur ça n'a pas marché
        	return;
        }
        
        try {
			sc.configureBlocking(false);
		} catch (IOException e1) {
			return;
		}           
        
        Debug.printDebug("incoming connection");
        
        
        
        ByteBuffer tmp = ByteBuffer.allocate(Client.BUFFER_SIZE);
        
        tmp.clear(); tmp.limit(Integer.BYTES);
        
        if( !readFullyWithTimeOut(tmp,sc)) { 
        	
        	Debug.printDebug("incoming connection failed 1");	
         	return; 
        }
        
        if( CR.AuthentificationPrivee.ordinal() != tmp.flip().getInt()) {
        	Debug.printDebug("incoming connection failed 2");	
        	return;
        	
        };
        
        tmp.clear(); tmp.limit(Integer.BYTES);
        
        if( !readFullyWithTimeOut(tmp,sc)) { 
        	Debug.printDebug("incoming connection failed 3");	
        	return;
        	
        }       
        
        int token = tmp.flip().getInt();
       
        tmp.clear(); tmp.limit(Integer.BYTES);
        if( !readFullyWithTimeOut(tmp,sc)) { 
        	
        	Debug.printDebug("incoming connection failed 4");	
        	return;

        }       

        int size = tmp.flip().getInt();
        tmp.clear();
        tmp.limit(size);
        
        if( !readFullyWithTimeOut(tmp,sc)) { 
        	Debug.printDebug("incoming connection failed 5");	
        	return;
        	
        }       
        
        String name = Client.UTF8_CHARSET.decode(tmp.flip()).toString();
        
        Debug.printDebug("Auth fully received 11 --> " + name + "/" + token);
        State st = UserList.authUserFromServer(token, name);
        
        Debug.printDebug("Auth fully received");
        
        if( st == State.WAITINGFORAUTH ) {
            Debug.printDebug("Auth fully received 1 -->" + st);       
            
	        SelectionKey sk;
			try {
				sk = sc.register(this.selector, SelectionKey.OP_WRITE);
			} catch (ClosedChannelException e) {
	        	Debug.printDebug("incoming connection failed 6");	
				return;
			}
	        // get the new selection key
	        UserContext usr = UserList.getUser(name);
	        usr.serverInfo(this, sk);
	        sk.attach(UserList.getUser(name));
	        usr.changeState(State.OPEN);
	        System.out.println("You're fully connected with "+ name + "!!!");
	        usr.putMessage(sayHello());
	        return;
	        
        }
        else {
            st = UserList.authUserFromServer(token, name);
            Debug.printDebug("Auth fully received 2 -->" + st);
        	try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return;
			}
        	//second chance 
            if( st == State.WAITINGFORAUTH) {
                
    	        SelectionKey sk;
    			try {
    				sk = sc.register(this.selector, SelectionKey.OP_WRITE);
    			} catch (ClosedChannelException e) {
    	        	Debug.printDebug("incoming connection failed 7");	
    				return;
    			}
    	        // get the new selection key
    	        UserContext usr = UserList.getUser(name);
    	        usr.serverInfo(this, sk);
    	        sk.attach(UserList.getUser(name));
    	        usr.changeState(State.OPEN);
    	        System.out.println("You're fully connected with "+ name + "!!!");
    	        usr.putMessage(sayHello());
    	        return;
    	        
            }
            else {
                System.out.println("Error in connection with "+ name + "/" + token + "!!!");
                UserList.removeUser(name);
            	return;
            }
        	 
        }
        
        
		
	}
	
	
	private Message sayHello() {
		
		List<Message> tmp = MessageUtils.createMessage("In private with " + Connect.getName(), ConnectionType.PRIVATE, Connect.getToken());	
		return tmp.get(0);
	}
	
	/**
	 * try to fill until there no remaining
	 * @param bf ByteBuffer
	 * @return true if success , false if not
	 */
	private boolean readFullyWithTimeOut(ByteBuffer bf, SocketChannel sc) {
		
		int read = 0;
		
		long time = System.currentTimeMillis();
		
			while(read != -1) {
				
				try {
					read = sc.read(bf);
				} catch (IOException e) {
					return false;
				}
					
				
				
				if( !bf.hasRemaining()) {
					return true;
				}
				
				if( (System.currentTimeMillis() - time) > timeOut) {
					return false;
				}
				
				
			}
			return false;

	}






	/***
	 *  Theses methods are here to help understanding the behavior of the selector
	 ***/

	private String interestOpsToString(SelectionKey key){
		if (!key.isValid()) {
			return "CANCELLED";
		}
		int interestOps = key.interestOps();
		ArrayList<String> list = new ArrayList<>();
		if ((interestOps&SelectionKey.OP_ACCEPT)!=0) list.add("OP_ACCEPT");
		if ((interestOps&SelectionKey.OP_READ)!=0) list.add("OP_READ");
		if ((interestOps&SelectionKey.OP_WRITE)!=0) list.add("OP_WRITE");
		return String.join("|",list);
	}

	public void printKeys() {
		Set<SelectionKey> selectionKeySet = selector.keys();
		if (selectionKeySet.isEmpty()) {
			System.out.println("The selector contains no key : this should not happen!");
			return;
		}
		System.out.println("The selector contains:");
		for (SelectionKey key : selectionKeySet){
			SelectableChannel channel = key.channel();
			if (channel instanceof ServerSocketChannel) {
				System.out.println("\tKey for ServerSocketChannel : "+ interestOpsToString(key));
			} else {
				SocketChannel sc = (SocketChannel) channel;
				System.out.println("\tKey for Client "+ remoteAddressToString(sc) +" : "+ interestOpsToString(key));
			}


		}
	}

	private String remoteAddressToString(SocketChannel sc) {
		try {
			return sc.getRemoteAddress().toString();
		} catch (IOException e){
			return "???";
		}
	}

	private void printSelectedKey() {
		if (selectedKeys.isEmpty()) {
			System.out.println("There were not selected keys.");
			return;
		}
		System.out.println("The selected keys are :");
		for (SelectionKey key : selectedKeys) {
			SelectableChannel channel = key.channel();
			if (channel instanceof ServerSocketChannel) {
				System.out.println("\tServerSocketChannel can perform : " + possibleActionsToString(key));
			} else {
				SocketChannel sc = (SocketChannel) channel;
				System.out.println("\tClient " + remoteAddressToString(sc) + " can perform : " + possibleActionsToString(key));
			}

		}
	}

	private String possibleActionsToString(SelectionKey key) {
		if (!key.isValid()) {
			return "CANCELLED";
		}
		ArrayList<String> list = new ArrayList<>();
		if (key.isAcceptable()) list.add("ACCEPT");
		if (key.isReadable()) list.add("READ");
		if (key.isWritable()) list.add("WRITE");
		return String.join(" and ",list);
	}
}

