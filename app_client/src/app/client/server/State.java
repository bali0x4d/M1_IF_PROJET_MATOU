package app.client.server;

public enum State {
	
	OPEN /* connected ready for all*/,
	CLOSED /* closed kill it */,
	REFUSED /* connection refused */,
	PENDING /* waiting for auth from main server */,
	WAITINGFORAUTH /*waiting for auth by user */,
	INIT /* first state for nothing */

}
