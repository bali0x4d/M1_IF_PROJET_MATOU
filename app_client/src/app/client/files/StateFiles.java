package app.client.files;

public enum StateFiles {
	
	ASKED,
	ACCEPTED,
	REFUSED,
	SENDING,
	RECEIVING

}
