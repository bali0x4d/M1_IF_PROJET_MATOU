package app.client.langue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * @author baran / landry
 * Language template 
 *
 */
public class Lang {
	
	/**
	 * locale language to use 
	 */
	private static Locale language = Locale.FRENCH;
	
	/**
	 * Wherre to find ressource for locale
	 */
	private static ResourceBundle rb  = ResourceBundle.getBundle("app/client/rsc/lang/sys",language);;
	
	
	/**
	 * Available locale for app
	 */
	private static Locale[] supportedLocale = {
	         Locale.FRENCH,
	         Locale.ENGLISH
	};
	
	
	/**
	 * To set available language 
	 * List it with supportedLang()
	 * @param lang int between 0 and supportedLocale.length
	 * @throws IllegalArgumentException if lang is out of bound
	 */
	public static void setLang(int lang) {
		
		if( lang < 0 || lang > supportedLocale.length ) {
			throw new IllegalArgumentException("Error local is not good ABORT");
		}
		
		language = supportedLocale[lang];
		rb = ResourceBundle.getBundle("app/client/rsc/lang/sys",language);
				
	}
	
	
	
	/**
	 * Return translation for the key with actual Locale
	 * @param trans get Key for translation
	 * @return String if key is not set return Translation is not there error
	 */
	public static String getTrans(String trans) {
		
		try {
			
			String tmp = rb.getString(trans);
			
			
			
			return tmp;
		}
		catch(MissingResourceException e){
			
			return "Translation is not there ";
		}
		
	}
	

	
	/**
	 * get a list with supportedLang
	 * @return List list of string message
	 */
	public static List<String> supportedLang() {		
		return Arrays.stream(supportedLocale).map(l -> l.toString()).collect(Collectors.toList());
	}
	
	/**
	 * Print to user the supportedLocal available 
	 */
	public static void printLang() {
		
		List<String> tmp = supportedLang();
		int i = 0;
		System.out.println("Default (0)");
		for(String t: tmp) {
			System.out.println(i+")"+t);
			i++;
		}
		
	}

}
