package app.client.rsc;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import app.client.client.Client;
import app.client.rfc.CR;
import app.client.rfc.ConnectionType;
import app.client.test.Debug;
import app.client.unique.Connect;

public class RequestRead {
	
	
	/**
	 * Give a complete bytebuffer with important part of packet
	 * @param cr RequestCode
	 * @param ct ConnectionType
	 * @param token TokenOfUSer
	 * @return ByteBuffer 
	 */
	public static ByteBuffer request(Integer cr, ConnectionType ct, Long token) {
						
			
				if( cr == CR.ReceptionMessageUnique.ordinal()) {
					/* token -1 , get from server */
					token = Long.valueOf(-1);
					ct = ConnectionType.PUBLIC;
					return RequestRead.uniqueMessagePub();					
				}
				else if( cr == CR.ReceptionMessageMultiple.ordinal()
						|| cr == CR.FinReceptionMessageMultiple.ordinal()) {
					token = Long.valueOf(-1);
					ct = ConnectionType.PUBLIC;
					return RequestRead.multipleMessagePub(cr);
				}
				else if( cr == CR.InformationConvPrivee.ordinal()) {
					
					ct = ConnectionType.PUBLIC;
					token = Long.valueOf(-1);
					
					return infoConvPriv();
					
				}
				else if( cr == CR.DemandeConversationPriveeServeur.ordinal()) {
					
					ct = ConnectionType.PUBLIC;
					token = Long.valueOf(-1);
					
					return demandeConvPrive();
					
				}
				else if( cr == CR.ListeSalle.ordinal()) {
					
					ct = ConnectionType.PUBLIC;
					token = Long.valueOf(-1);
					
					return listeOfRoom();
					
				}
				else if ( cr == CR.ReponseRequeteConvPrivee.ordinal()) {
					
					ct = ConnectionType.PUBLIC;
					token = Long.valueOf(-1);
					
					return reqPriv();
					
				}
				else if ( cr == CR.ReponseSalle.ordinal()) {
					
					ct = ConnectionType.PUBLIC;
					token = Long.valueOf(-1);
					
					return reqSalle();
					
				}					
			  else {
					
					Debug.printDebug("CR not found in request read");
				}
			
			
		return null;
	}
	


	/**
	 * @return state of asking room it s int
	 */
	private static ByteBuffer reqSalle() {
		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		bf.clear();
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}
		
		return bf;
	}



	/**
	 * anayse private query
	 * @param state state of value
	 * @return bytebuffer with name
	 */
	private static ByteBuffer reqPriv() {
		
		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		bf.clear();
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}
		
		int state = bf.flip().getInt();

		bf.clear();
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}		
		
		int size = bf.flip().getInt();
		
		bf.clear();
		bf.limit(size+Integer.BYTES);
		bf.putInt(state);
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}	
		
		
		return bf;
	}


	private static ByteBuffer listeOfRoom() {

		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		bf.clear();
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}		
		
		bf.flip();
		int nbSalle = bf.getInt();
		
		bf.clear();
		bf.limit(Integer.BYTES * (nbSalle * 3));
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}			
		
		
		return bf;
		
	}


	private static ByteBuffer demandeConvPrive() {

		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		bf.clear();
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}
		
		bf.flip();
		int size = bf.getInt();		
		
		
		bf.clear();
		bf.limit(size);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}
		
		return bf;
		
		
	}


	private static ByteBuffer infoConvPriv() {
		
		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		bf.clear();
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}
		
		bf.flip();
		int size = bf.getInt();		
		
		
		bf.clear();
		bf.limit(size);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
		}
		
		return bf;
		
		
	}
	

	
	private static ByteBuffer multipleMessagePub(int cr) {
		
		//we've got an unique message
		ByteBuffer bf = null;
		
		
		if( cr == CR.ReceptionMessageMultiple.ordinal()) {
			// fill maximum
			Debug.printDebug("10 recu");
			bf = ByteBuffer.allocate(Client.BUFFER_SIZE-(Integer.BYTES*2));
			bf.clear();
			if(!readFully(bf)) {
				Connect.closedByServer();
				return null;
			}
			Debug.printDebug("10 traité");
			return bf;
		}
		
		bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		// get size message
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
			
		}
		
		bf.flip();
		int size = bf.getInt();
		
		
		if( size > Client.BUFFER_SIZE) {
			// throw it invalid size
			return null;
		}
		
		bf.clear();
		bf.limit(size);
		
		if(!readFully(bf)) {
			Debug.printDebug("Erreur ump n2");
			Connect.closedByServer();
			return null;
			
		}
		
		Debug.printDebug("MESSAGE RECU CR 11 TRAITE TAILLE SIZE: " + size);
		
		return bf;
	}


	private static ByteBuffer uniqueMessagePub() {
		
		//we've got an unique message
		ByteBuffer bf = ByteBuffer.allocate(Client.BUFFER_SIZE);
		
		bf.clear();
		
		// get size message
		bf.limit(Integer.BYTES);
		
		if(!readFully(bf)) {
			Connect.closedByServer();
			return null;
			
		}
		
		bf.flip();
		int size = bf.getInt();
		
		
		if( size > Client.BUFFER_SIZE) {
			// throw it invalid size
			return null;
		}
		
		bf.clear();
		bf.limit(size);
		
		if(!readFully(bf)) {
			Debug.printDebug("Erreur ump n1");
			Connect.closedByServer();
			return null;
			
		}
		
		Debug.printDebug("MESSAGE RECU CR 9 TRAITE TAILLE SIZE: " + size);
		
		return bf;
	}
	

	
	private static boolean readFully(ByteBuffer bf) {
		
		SocketChannel sc = Connect.getSC();
		int read;
		try {
			read = sc.read(bf);
		} catch (IOException e) {				
			return false;
		}
			
			while(read != -1) {
				
			try {
				read = sc.read(bf);
			} catch (IOException e) {
				return false;
			}
				
				if( !bf.hasRemaining()) {
					return true;
				}
				
				
			}
			
			if(read == -1) {
				// connection closed
				Connect.closedByServer();
			}
			
		    return false;
		
		
	}
	
}
