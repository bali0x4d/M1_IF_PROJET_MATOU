package app.client.rsc;

import java.nio.ByteBuffer;

import app.client.client.Client;
import app.client.rfc.ConnectionType;

/**
 * simple message classe , it contains request code and a complete message to send 
 * Exemple message for CR = SEND UNIQUE MESSAGE ,   message have to be CR/TOKEN/SIZE/MESSAGE
 * @author bbuyuk
 *
 */
public class Message {
	
	private static int BUFFER_SIZE = Client.BUFFER_SIZE;
	private int code = -1;
	private long from;
	private ByteBuffer data = ByteBuffer.allocate(BUFFER_SIZE);
	private ConnectionType connType;
	private int state = 0;
	private String info = null;
	
	
	/**
	 * get info
	 * @param info get info
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getInfo() {
		return this.info;
	}
	
	/**
	 * State 0-1 for what you want
	 * @param state int for the state
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * get the state value
	 * @return int
	 */
	public int getState() {
		return this.state;
	}
	
	private Message(int code, ByteBuffer d, ConnectionType ct, long tokenFrom) {
		
		this.code = code;
		this.data.put(d);
		this.connType = ct;
		this.from = tokenFrom;
	}
	
	/**
	 * Create a message, src buffer copied to dst buffer,
	 *  you can delete src buffer after creating
	 * @param code coque request from rfc
	 * @param d a bufffer dont flip the src
	 * @param ct connectionType
	 * @return Message 
	 */
	public static Message buildMessage(int code, ByteBuffer d, ConnectionType ct, long tokenFrom) {
		
		d.flip();
		if( d.limit() > (BUFFER_SIZE)) {
			// error 
			return null;
			
		}
		return new Message(code,d,ct,tokenFrom);
	}
	
	/**
	 * Get buffer from message
	 * @return ByteBuffer 
	 */
	public ByteBuffer getBuffer() {
		return this.data;
	}
	
	/**
	 * @return int get the request code 
	 */
	public int getCR() {
		return this.code;
	}
	
	/**
	 * get token from sender
	 * @return long
	 */
	public long getFrom() {
		return from;
	}

	/** 
	 * set sender token
	 * @param from
	 */
	public void setFrom(long from) {
		this.from = from;
	}

	/**
	 * @param newCR change the request code
	 */
	public void changeCR(int newCR) {
		this.code = newCR;
		int olPos = this.data.position();
		
		this.data.position(0);
		
		this.data.putInt(newCR);
		
		this.data.position(olPos);
		 
	}

	/**
	 * Get Connexion Type
	 * @return
	 */
	public ConnectionType getConnType() {
		return connType;
	}

	/**
	 * Change connType
	 * @param connType
	 */
	public void setConnType(ConnectionType connType) {
		this.connType = connType;
	}
	
	

}
