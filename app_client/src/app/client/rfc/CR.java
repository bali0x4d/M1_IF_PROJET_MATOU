package app.client.rfc;

public enum CR {
	 Authentification,
	 Autorisation,
	 DemandeSalle,
	 ListeSalle,
	 ConnexionSalle,
	 ReponseSalle,
	 EnvoieMessageUnique,
	 EnvoieMessageMultiple,
	 FinMessageMultiple,
	 ReceptionMessageUnique,
	 ReceptionMessageMultiple,
	 FinReceptionMessageMultiple,
	 DeconnexionSalleConvPrivee,
	 DeconnexionServeurFinAppli,
	 DemandeConversationPriveeClient,
	 DemandeEnvoieFichier,
	 ReceptionFichier,
	 FinFichier,
	 InformationConvPrivee,
	ReponseRequeteConvPrivee,
	 DemandeConversationPriveeServeur,
	 ListerUtilisateurServeurPublic,
	AuthentificationPrivee,
	PortPrivee,
	 RequeteFichier
}
