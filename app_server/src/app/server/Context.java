package app.server;

import app.client.rfc.CR;
import tools.managers.ConnectionManager;
import tools.managers.MessageManager;
import tools.operations.Buffers;
import tools.operations.Operations;
import tools.operations.PrivateConversationOperations;
import tools.readers.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.function.Function;
import java.util.logging.Logger;

import static app.server.ServerMain.silentlyClose;

public class Context {

    static private int BUFFER_SIZE = 1_024;
    static private int MAX_CLIENTS = 24;
    static private Logger logger = Logger.getLogger(Context.class.getName());

    final private SelectionKey key;
    final public SocketChannel sc;
    private final ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
    final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
    final private Queue<ByteBuffer> queue = new LinkedList<>();
    final private ServerMain server;
    final public int token;
    private boolean inputClosed = false;
    private boolean processing = false;
    private int codeRequete = -1;
    public int room;
    public String pseudo;
    public int port;
    private final static Map<Integer, Function<ByteBuffer, Reader>> readerMap = new HashMap<>();
    static {
        Function<ByteBuffer, Reader> functionConnection = (bb)-> new StringReader((ByteBuffer)bb);
        readerMap.put(0 , functionConnection);

        Function<ByteBuffer, Reader> functionTKM = (bb)-> TokenMessageReader.createTokenMessageReader((ByteBuffer)bb);
        readerMap.put(6 , functionTKM);

        Function<ByteBuffer, Reader> functionMMBR = (bb)-> MultipleMessageBodyReader.createMultipleMessageBodyReader((ByteBuffer)bb);
        readerMap.put(7 , functionMMBR);

        Function<ByteBuffer, Reader> functionEndBodyReader = (bb)-> TokenMessageReader.createTokenMessageReader((ByteBuffer)bb);
        readerMap.put(8 , functionEndBodyReader);
    }
    Context(ServerMain server, SelectionKey key, int token){
        this.key = key;
        this.sc = (SocketChannel) key.channel();
        this.server = server;
        this.token = token;
        this.room = 1;
    }

    /**
     * Send a buffer to a queue created from
     * the bbin
     */
    private void processIn() {
        if (!processing){
            try {
                codeRequete = RequestCodeReader.get(bbin);
                processing = true;
            }catch (RuntimeException RE){
                logger.info("REQUEST CODER ERROR !");
            }

        }
        if(!particularCodeRequest(codeRequete)){
            try{
                Reader reader = readerMap.get(codeRequete).apply(bbin);
                for (; ; ) {
                    Reader.ProcessStatus status = reader.process();
                    switch (status) {
                        case DONE:
                            try{
                                Operations.broadcast(new MessageManager((String[]) reader.get(), pseudo).get(), this.room, server);
                            }catch (RuntimeException re ){
                                logger.severe("Buffer malformé");
                            }

                            reader.reset();
                            codeRequete = -1;
                            processing=false;
                            return;
                        case REFILL:
                            return;
                        case ERROR:
                            silentlyClose(sc);
                            return;
                    }
                }
            }catch (NullPointerException | ClassCastException npe){
                logger.info("Process In ERROR");
            }
        }

    }

    /**
     * Compute the write methode associated to the Code Request
     * @param codeRequete int
     * @return true if a methode is applied else false
     */
    private boolean particularCodeRequest(int codeRequete){
        Reader reader;
        switch (codeRequete){
            case 0:
                greetings(server);
                processing=false;
                return true;
            case 2:
                Operations.listeSalle(this, server);
                processing=false;
                return true;

            case 4:
                Boolean accepted = connectionToARoom(codeRequete);
                if (accepted != null)
                    return accepted;
            case 14:
                bbin.flip();
                reader = new StringReader(bbin);
                Boolean pc = PrivateConversationOperations.privateConversation(reader, this, server);
                processing=false;
                return pc;

            case 19:
                Boolean answered = requestResponse(codeRequete);
                if (answered != null) return answered;

            case 21:
                Operations.getNameOfClientConnected(this, server);
                processing=false;
                return true;
            case 23:
                Boolean seted = setPrivatePort(codeRequete);
                if (seted != null)
                    return seted;
            default:
                return false;
        }
    }

    /**
     * try to respond to a user request
     * @param codeRequete request code
     * @return true if it succeed
     */
    private Boolean requestResponse(int codeRequete) {
        Reader reader;
        try{
            reader = PrivateConvResponseMessageReader.createPrivateConvResponseMessageReader(bbin);
            for (; ; ) {
                Reader.ProcessStatus status = reader.process();
                switch (status) {
                    case DONE:
                        String[] response = (String[])reader.get();
                        String askedClient = response[3];
                        if(askedClient.equals(this.pseudo)){
                            PrivateConversationOperations.cancelPrivateConv(askedClient, this);
                        }
                        responsePrivateConv(response, askedClient);
                        processing=false;
                        return true;
                    case REFILL:
                        break;
                    case ERROR:
                        silentlyClose(sc);
                        processing=false;
                        return false;
                }
            }
        }catch (NullPointerException npe){
            logger.info("Process In ERROR");
        }
        return null;
    }

    /**
     * set the user Private port
     * @param codeRequete port request code
     * @return true if it succeed
     */
    private Boolean setPrivatePort(int codeRequete) {
        Reader reader;
        try{
            reader = new IntReader(bbin);
            for (; ; ) {
                Reader.ProcessStatus status = reader.process();
                switch (status) {
                    case DONE:
                        Operations.setPort(this,(int)reader.get());
                        processing=false;
                        return true;
                    case REFILL:
                        break;
                    case ERROR:
                        silentlyClose(sc);
                        processing = false;
                        return false;
                }
            }
        }catch (NullPointerException npe){
            logger.info("Process In ERROR");
        }
        return null;
    }

    /**
     * Try to connect user to a room
     * @param codeRequete connection room request code
     * @return true if connection was a success
     */
    private Boolean connectionToARoom(int codeRequete) {
        Reader reader;
        try{
            reader = new IntReader(bbin);
            for (; ; ) {
                Reader.ProcessStatus status = reader.process();
                switch (status) {
                    case DONE:
                        Operations.setNewRoom(this,(int)reader.get(),server);
                        processing=false;
                        return true;
                    case REFILL:
                        break;
                    case ERROR:
                        silentlyClose(sc);
                        processing = false;
                        return false;
                }
            }
        }catch (NullPointerException npe){
            logger.info("Process In ERROR");
        }
        return null;
    }


    /****************************************************************
     ***********OPERATION ASSOCIEES AUX CONVERSATIONS PRIVEE ********
     ****************************************************************/

    /**
     *
     * @param response String tab containing the right informations to perform this method
     * @param askedClient Client that have asked to initiate a private conversation
     */
    private void responsePrivateConv(String[] response, String askedClient) {
        for(SelectionKey key : server.selector.keys()){
            Context client = (Context)key.attachment();
            if(client != null){
                if(askedClient.equals(client.pseudo)){
                    ByteBuffer buffer = Buffers.getBufferPrivateConvResponse(this,response[1]);
                    Operations.sendResponse(buffer, client);
                    PrivateConversationOperations.sendIpPortToClients(client, this);
                }
            }
        }
    }

    private void setPseudo(String pseudo){
        this.pseudo = pseudo;
    }

    public void switchRoom(int newRoom){
        this.room = newRoom;
    }

    /**
     * Allow or not to connect to the server
     * @param server Servermain
     */
    private void greetings(ServerMain server) {
        bbin.flip();
        try {
            StringReader reader = new StringReader(bbin);
            reader.process();
            String pseudo = (String)reader.get();
            if(server.clientName.contains(pseudo)){
                Operations.sendResponse(new ConnectionManager(CR.Autorisation,1, this.token).get(), this);
                return;
            }
            for(int index = 1; index <= server.nbRoom; index++){
                if(server.rooms.get(index)>= MAX_CLIENTS){
                    continue;
                }
                if(server.rooms.get(index)>= MAX_CLIENTS && index == server.nbRoom){
                    Operations.sendResponse(new ConnectionManager(CR.Autorisation,3, this.token).get(), this);
                    return;
                }
            }
            setPseudo(pseudo);
            Operations.sendResponse(new ConnectionManager(CR.Autorisation,0,this.token).get(), this);
            Operations.helloWorld(this);
        }catch (RuntimeException ie){
            logger.info("Erreur inconnu");
            Operations.sendResponse(new ConnectionManager(CR.Autorisation,4,this.token).get(), this);
        }

        processing = false;

    }

    /**
     * Add a message to the message queue, tries to fill bbOut and updateInterestOps
     *
     * @param message Buffer containing a message
     */
    public void queueMessage(ByteBuffer message) {
        queue.add(message);
        processOut();
        updateInterestOps();
    }

    /**
     * Try to fill bbout from the message queue
     *
     * If it's possible, the message is put inn
     * bbout from the peeked queue
     */
    private void processOut() {
        while ( queue.size() > 0 && bbout.hasRemaining()) {
            ByteBuffer message = queue.peek();
            message.flip();
            if (bbout.remaining() >= message.remaining()) {
                bbout.put(message);
                queue.poll();
            }
        }
    }


    /**
     * Update the interestOps of the key looking
     * only at values of the boolean closed and
     * of both ByteBuffers.
     *
     * The convention is that both buffers are in write-mode before the call
     * to updateInterestOps and after the call.
     * Also it is assumed that process has been be called just
     * before updateInterestOps.
     */

    private void updateInterestOps() {
        int ops = 0;
        if(bbin.hasRemaining() && !inputClosed){
            ops |= SelectionKey.OP_READ;
        }
        if(bbout.position() > 0 || !queue.isEmpty()){
            ops |= SelectionKey.OP_WRITE;
        }
        if (ops == 0){
            silentlyClose(sc);
        }else {
            key.interestOps(ops);
        }
    }

    /**
     * Performs the read action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doRead and after the call
     *
     * @throws IOException sq
     */

    void doRead() throws IOException {
        if(sc.read(bbin) == -1){
            inputClosed=true;
        }
        processIn();
        updateInterestOps();
    }

    /**a
     * Performs the write action on sc
     *
     * The convention is that both buffers are in write-mode before the call
     * to doWrite and after the call
     *
     * @throws IOException io
     */
    void doWrite() throws IOException {
        bbout.flip();
        sc.write(bbout);
        bbout.compact();
        processOut();
        updateInterestOps();
    }
}