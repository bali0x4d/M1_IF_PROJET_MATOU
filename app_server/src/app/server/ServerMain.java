package app.server;

import tools.managers.CommandManager;
import tools.operations.Operations;
import tools.operations.RoomMap;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerMain {

    static private int MAX_CLIENTS = 24;
    static private Logger logger = Logger.getLogger(ServerMain.class.getName());

    private final ServerSocketChannel serverSocketChannel;
    public final Selector selector;
    private final Set<SelectionKey> selectedKeys;
    public final int nbRoom;
    public Map<Integer, Integer> rooms;
    ArrayList<String> clientName;
    private final Random tokenGenerator;

    private ServerMain(int port) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(port));
        selector = Selector.open();
        selectedKeys = selector.selectedKeys();
        this.nbRoom = 4;
        this.rooms = RoomMap.initRooms();
        this.clientName = new ArrayList<>();
        this.tokenGenerator = new Random(MAX_CLIENTS);
    }

    private void launch() throws IOException {
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        CommandManager.commandThread(this);
        while (!Thread.interrupted()) {
            printKeys();
            System.out.println("Starting select");
            selector.select();
            System.out.println("Select finished");
            printSelectedKey();
            processSelectedKeys();
            selectedKeys.clear();
            rooms = RoomMap.realocateRooms(this);
        }
    }

    /**
     * Switch to Write or read each key of our selector
     * @throws IOException io
     */
    private void processSelectedKeys() throws IOException {
        for (SelectionKey key : selectedKeys) {
            if (key.isValid() && key.isAcceptable()) {
                doAccept();
            }
            try {
                if (key.isValid() && key.isWritable()) {
                    ((Context) key.attachment()).doWrite();
                }
                if (key.isValid() && key.isReadable()) {
                    ((Context) key.attachment()).doRead();
                }
            } catch (IOException e) {
                logger.log(Level.INFO,"Connection closed with client due to IOException",e);
                silentlyClose(key);
            }
        }
    }

    /**
     * Accept a new client and add it to
     * our selector
     * @throws IOException io
     */
    private void doAccept() throws IOException {
        SocketChannel sc;
        if((sc=serverSocketChannel.accept())==null){
            return;
        }
        sc.configureBlocking(false);
        SelectionKey clientKey = sc.register(selector,SelectionKey.OP_READ);
        Context context = new Context(this, clientKey, tokenGenerator.nextInt(65_555));
        clientKey.attach(context);
        Operations.setClientRoom(context, this);
        clientName.add(context.pseudo);
    }

    private void silentlyClose(SelectionKey key) {
        Channel sc = key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }


    public static void main(String[] args) throws NumberFormatException, IOException {
        if (args.length!=1){
            usage();
            return;
        }
        new ServerMain(Integer.parseInt(args[0])).launch();
    }

    private static void usage(){
        System.out.println("Usage : ServerChat port");
    }

    /***
     *  Theses methods are here to help understanding the behavior of the selector
     ***/

    private String interestOpsToString(SelectionKey key){
        if (!key.isValid()) {
            return "CANCELLED";
        }
        int interestOps = key.interestOps();
        ArrayList<String> list = new ArrayList<>();
        if ((interestOps&SelectionKey.OP_ACCEPT)!=0) list.add("OP_ACCEPT");
        if ((interestOps&SelectionKey.OP_READ)!=0) list.add("OP_READ");
        if ((interestOps&SelectionKey.OP_WRITE)!=0) list.add("OP_WRITE");
        return String.join("|",list);
    }

    private void printKeys() {
        Set<SelectionKey> selectionKeySet = selector.keys();
        if (selectionKeySet.isEmpty()) {
            System.out.println("The selector contains no key : this should not happen!");
            return;
        }
        System.out.println("The selector contains:");
        for (SelectionKey key : selectionKeySet){
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tKey for ServerSocketChannel : "+ interestOpsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tKey for Client "+ remoteAddressToString(sc) +" : "+ interestOpsToString(key));
            }
        }
    }

    private String remoteAddressToString(SocketChannel sc) {
        try {
            return sc.getRemoteAddress().toString();
        } catch (IOException e){
            return "???";
        }
    }

    private void printSelectedKey() {
        if (selectedKeys.isEmpty()) {
            System.out.println("There were not selected keys.");
            return;
        }
        System.out.println("The selected keys are :");
        for (SelectionKey key : selectedKeys) {
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tServerSocketChannel can perform : " + possibleActionsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tClient " + remoteAddressToString(sc) + " can perform : " + possibleActionsToString(key));
            }

        }
    }

    private String possibleActionsToString(SelectionKey key) {
        if (!key.isValid()) {
            return "CANCELLED";
        }
        ArrayList<String> list = new ArrayList<>();
        if (key.isAcceptable()) list.add("ACCEPT");
        if (key.isReadable()) list.add("READ");
        if (key.isWritable()) list.add("WRITE");
        return String.join(" and ",list);
    }

    static void silentlyClose(SelectableChannel sc) {
        if (sc==null)
            return;
        try {
            sc.close();
        } catch (IOException e) {
            // silently ignore
        }
    }

    public void shutdown() {
        try {
            serverSocketChannel.close();
        } catch (IOException e) {
            //
        }
    }

    public void shutdownNow() {
        try {
            serverSocketChannel.close();
            for(SelectionKey key : selector.keys()){
                Context client = (Context)key.attachment();
                if(client != null){
                    client.sc.close();
                }
            }
        } catch (IOException e) {
            //
        }

    }
}
