package tools.operations;

import app.server.Context;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class Buffers {
    /**
     * Create a simple buffer
     * @return buff with the right int in it
     */
    static ByteBuffer roomOkBuffer(){
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(5);
        return buffer;
    }

    /**
     * Create a simple buffer
     * @return buff with the right int in it
     */
    static ByteBuffer roomAlreadyInBuffer(){
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(0);
        return buffer;
    }

    /**
     * Create a simple buffer
     * @return buff with the right int in it
     */
    static ByteBuffer roomFullBuffer(){
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(1);
        return buffer;
    }

    /**
     * Create a simple buffer
     * @return buff with the right int in it
     */
    static ByteBuffer roomUnknown(){
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(2);
        return buffer;
    }

    /**
     * Create a simple buffer
     * @return buff with the right int in it
     */
    public static ByteBuffer getBufferPrivateConvResponse(Context context, String s) {
        ByteBuffer bufferPseudo = Charset.forName("UTF-8").encode(context.pseudo);
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES*3 + bufferPseudo.remaining());
        buffer.putInt(19);
        buffer.putInt(Integer.parseInt(s));
        buffer.putInt(bufferPseudo.remaining());
        buffer.put(bufferPseudo);
        return buffer;
    }
}
