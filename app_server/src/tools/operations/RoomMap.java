package tools.operations;

import app.server.Context;
import app.server.ServerMain;

import java.nio.channels.SelectionKey;
import java.util.HashMap;

public class RoomMap {
    /* ************* ROOM MAP OPERATIONS *******************/
    /**
     * Initiate the map containing our rooms
     * @return HashMap
     */
    public static HashMap<Integer, Integer> initRooms(){
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(1,0);
        map.put(2,0);
        map.put(3,0);
        map.put(4,0);
        return map;
    }

    /**
     * This method is call to relocate our client in the right room of
     * rh RoomMap
     * @return HashMap
     */
    public static HashMap<Integer, Integer> realocateRooms(ServerMain serverMain){
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(1,0);
        map.put(2,0);
        map.put(3,0);
        map.put(4,0);

        for (SelectionKey key : serverMain.selector.keys()) {
            Context client = (Context) key.attachment();
            if (client != null) {
                map.put(client.room, map.get(client.room)+1);
            }

        }
        return map;
    }
}
