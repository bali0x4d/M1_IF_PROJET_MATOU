package tools.operations;

import app.server.Context;
import app.server.ServerMain;
import tools.readers.Reader;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.charset.Charset;

public class PrivateConversationOperations {
    /**
     * Send to both information to connect to each other
     * @param client info
     * @param client2 info
     */
    public static void sendIpPortToClients(Context client, Context client2) {
        try {
            ByteBuffer bufferPseudo2 = Charset.forName("UTF-8").encode(client.pseudo);
            ByteBuffer bufferPseudo1 = Charset.forName("UTF-8").encode(client2.pseudo);

            ByteBuffer buffer1 = setClientInfos(client, bufferPseudo2);
            Operations.sendResponse(buffer1, client2);

            ByteBuffer buffer2 = setClientInfos(client2, bufferPseudo1);
            Operations.sendResponse(buffer2, client);

        } catch (IOException e) {
            // logger.info("Error in socket adress");
        }
    }

    /**
     * Create a buffer contening the client info
     * Token, IpAdress, pseudo, port
     * @param client asking client context
     * @param bufferPseudo2 asked client pseudo
     * @return ByteBuffer
     * @throws IOException ie
     */
    private static ByteBuffer setClientInfos(Context client, ByteBuffer bufferPseudo2) throws IOException {
        InetSocketAddress socketAddress = (InetSocketAddress)client.sc.getRemoteAddress();
        InetAddress inetAddress = socketAddress.getAddress();
        String ipAdress = inetAddress.getHostAddress();
        int port = client.port;

        ByteBuffer tmpAdress = Charset.forName("UTF-8").encode(ipAdress);
        ByteBuffer tmptoken = Charset.forName("UTF-8").encode(String.valueOf(client.token));
        ByteBuffer tmpPort1 = Charset.forName("UTF-8").encode(String.valueOf(port));
        ByteBuffer buffer1 = ByteBuffer.allocate(Integer.BYTES *2 + bufferPseudo2.remaining() + tmptoken.remaining() + tmpPort1.remaining() +tmpAdress.remaining() +3);
        buffer1.putInt(18);
        buffer1.putInt( bufferPseudo2.remaining() + tmptoken.remaining()+ tmpPort1.remaining() +tmpAdress.remaining() +3);
        buffer1.put(bufferPseudo2);
        buffer1.put(Charset.forName("UTF-8").encode(":"));
        buffer1.put(tmptoken);
        buffer1.put(Charset.forName("UTF-8").encode(":"));
        buffer1.put(tmpAdress);
        buffer1.put(Charset.forName("UTF-8").encode(":"));
        buffer1.put(tmpPort1);
        return buffer1;
    }

    /**
     * Check the asked client respond and perfom the right method
     * @param reader reader
     * @param context context
     * @param server server
     * @return true if connection is accepted, false if not
     */
    public static boolean privateConversation(Reader reader, Context context, ServerMain server) {
        reader.process();
        String askedClient = (String) reader.get();

        if (askedClient.equals(context.pseudo)) {
            cancelPrivateConv(askedClient, context);
            return false;
        }

        if (askPrivateConv(askedClient, context, server)) {
            return true;
        }
        cancelPrivateConv(askedClient, context);
        return false;
    }

    /**
     * Abord te private conversation procedure
     * @param askedClient asked client
     * @param context asking client
     */
    public static void cancelPrivateConv(String askedClient, Context context) {
        Operations.manageAndSend(new String[]{"19"}, askedClient, context);
    }

    /**
     * Asked a client to open a private conversaton
     * @param askedClient asked client
     * @param context asking client
     * @param server ServerMain
     * @return ByteBuffer containing the response
     */
    private static boolean askPrivateConv(String askedClient, Context context, ServerMain server) {
        for(SelectionKey key : server.selector.keys()){
            Context client = (Context)key.attachment();
            if(client != null){
                if(askedClient.equals(client.pseudo)){
                    Operations.manageAndSend(new String[]{"20"}, context.pseudo, client);
                    return true;
                }
            }
        }
        return false;
    }
}
