package tools.operations;

import app.server.Context;
import app.server.ServerMain;
import tools.managers.MessageManager;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.charset.Charset;
import java.util.logging.Logger;

public class Operations {

    static private int MAX_CLIENTS = 24;
    static private Logger logger = Logger.getLogger(Context.class.getName());

    /**
     * Add a message saying yes or no in the
     * queue of a specific client
     *
     * @param connectionMessage Buffer containing the response
     * @param context Context of a client
     */
    public static void sendResponse(ByteBuffer connectionMessage, Context context) {
        if(context!=null){
            context.queueMessage(connectionMessage);
        }
    }

    /**
     * Greetings to a newly connected client
     * @param context Context of the client
     */
    public static void helloWorld(Context context){
        if(context!=null){
            ByteBuffer tmp = Charset.forName("UTF-8").encode("Hello World! salle: "+String.valueOf(context.room));
            ByteBuffer hello = ByteBuffer.allocate(Integer.BYTES*2 + tmp.remaining());
            hello.putInt(9);
            hello.putInt(tmp.remaining());
            hello.put(tmp);
            context.queueMessage(hello);
        }
    }

    public static void getNbClientConnected(Context context, ServerMain server){
        if(context!=null) {
            ByteBuffer sallesBuffer = ByteBuffer.allocate(Integer.BYTES * 17);
            sallesBuffer.putInt(3);
            sallesBuffer.putInt(context.room);
            for (int index = 1; index <= server.nbRoom; index++) {
                sallesBuffer.putInt(server.rooms.get(index));
                sallesBuffer.putInt(MAX_CLIENTS);
                context.queueMessage(sallesBuffer);
            }
        }
    }

    /**
     * Send to the asking client wich client are connected to the room
     * @param context context of the asking client
     * @param server ServerMain
     */
    public static void getNameOfClientConnected(Context context, ServerMain server){
        if(context!=null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Liste des utilisateur connectés: \n");
            for (SelectionKey key : server.selector.keys()) {
                Context client = (Context) key.attachment();
                if (client != null) {
                    if (client.room == context.room) {
                        sb.append("- ").append(client.pseudo).append("\n");
                    }
                }
            }
            try{
                ByteBuffer clientName = Charset.forName("UTF-8").encode(sb.toString());
                ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES*2 + clientName.remaining());
                buffer.putInt(9);
                buffer.putInt(clientName.remaining());
                buffer.put(clientName);
                context.queueMessage(buffer);
            }catch (RuntimeException re){
                logger.info("Malformation dans le buffer listant les client");
            }
        }
    }

    /**
     * Add a message to all connected clients queue
     *
     * @param message Buffer containing our message
     */
    public static void broadcast(ByteBuffer message, int room, ServerMain server) {
        for (SelectionKey key : server.selector.keys()) {
            Context client = (Context) key.attachment();
            if (client != null) {
                if (client.room == room) {
                    client.queueMessage(message.duplicate());
                }
            }
        }
    }

    /**
     * Perform the right action to the input buffer then send him to
     * the right cleint
     * @param infoBuffer Info in the buffer
     * @param pseudo pseudo of te client
     * @param context context of the client
     */
    public static void manageAndSend(String[] infoBuffer, String pseudo, Context context) {
        MessageManager manager = new MessageManager(infoBuffer, pseudo);
        try {
            Operations.sendResponse(manager.get(), context);
        }catch (RuntimeException re){
            logger.severe("Buffer malformé");
        }
    }

    /***************************************************************
    ***********OPERATION ASSOCIEES AUX SALLE ***********************
    ****************************************************************/

    /**
     * Send to the asking client infos about te rooms
     * @param context Context of the asking client
     * @param server ServerMain
     */
    public static void listeSalle(Context context, ServerMain server) {
        if(context!=null){
            ByteBuffer sallesBuffer = ByteBuffer.allocate(Integer.BYTES*2+Integer.BYTES*(server.nbRoom*3));
            sallesBuffer.putInt(3);
            sallesBuffer.putInt(server.nbRoom);
            for(int index = 1; index <= server.nbRoom; index++){
                sallesBuffer.putInt(index);
                sallesBuffer.putInt(server.rooms.get(index));
                sallesBuffer.putInt(MAX_CLIENTS);
            }
            context.queueMessage(sallesBuffer);
        }
    }

    /**
     * Switch a client to a new room
     * @param context context of the asking client
     * @param newRoom asked room
     * @param server ServerMain
     */
    public static void setNewRoom(Context context, int newRoom, ServerMain server){
        if(context!=null){
            if(context.room != newRoom){
                context.switchRoom(newRoom);
                sendResponse(Buffers.roomOkBuffer(), context);
            }
            else if(context.room == newRoom){
                sendResponse(Buffers.roomAlreadyInBuffer(), context);
            }else if(server.rooms.get(newRoom)==MAX_CLIENTS){
                sendResponse(Buffers.roomFullBuffer(), context);
            }else {
                sendResponse(Buffers.roomUnknown(), context);
            }
        }
    }

    /**
     * Set a room to a client
     * @param context client context
     * @param server ServerMain
     */
    public static void setClientRoom(Context context, ServerMain server){
        for (int index = 1; index <= server.nbRoom; index++){
            if (server.rooms.get(index)<MAX_CLIENTS){
                context.switchRoom(index);
                server.rooms.put(index, server.rooms.get(index)+1);
                return;
            }
        }
    }

    /**
     * Set a port to a client
     * @param context client context
     * @param port client port
     */
    public static void setPort(Context context, int port) {
        context.port = port;
    }

}
