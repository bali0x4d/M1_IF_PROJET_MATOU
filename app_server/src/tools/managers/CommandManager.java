package tools.managers;

import app.server.ServerMain;

import java.util.Scanner;

public class CommandManager {
    /**
     * Thread with the scanner to read the user
     * inputs
     * @param server ServerMain
     */
    public static void commandThread(ServerMain server){
        Thread command = new Thread(()->{
            String cmd;
            while (!Thread.interrupted()) {
                try (Scanner scanner = new Scanner(System.in)) {
                    while (scanner.hasNextLine()){
                        cmd = scanner.nextLine();
                        commands(server, cmd);
                    }
                }
            }
        });
        command.start();
    }

    /**
     * Perform the command in the cmd parameter
     * @param server ServerMain
     * @param cmd String containing the input of the user
     */
    private static void commands(ServerMain server, String cmd){
        switch (cmd){
            case "SHUTDOWN":
                System.out.println("SHUTDOWN");
                server.shutdown();
                break;

            case "SHUTDOWNNOW":
                System.out.println("SHUTDOWN NOW");
                server.shutdownNow();
                return ;
            default :
                System.out.println("Choose\nSHUTDOWN\nSHUTDOWNNOW");
        }
    }
}
