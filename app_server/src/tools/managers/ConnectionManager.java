package tools.managers;

import app.client.rfc.CR;

import java.nio.ByteBuffer;

public class ConnectionManager {
    private final CR cr;
    private final int state;
    private final int token;

    public ConnectionManager(CR cr, int state, int token){
        this.cr =CR.Autorisation;
        this.state = state;
        this.token = token;
    }

    /**
     * return a buffer sized and full for a connection response
     * @return ByteBuffer
     */
    public ByteBuffer get(){
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES*3);
        buffer.putInt(cr.ordinal());
        buffer.putInt(state);
        buffer.putInt(token);
        return buffer;
    }
}
