package tools.managers;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class MessageManager {
    private final String[] infoMessage;
    private final String pseudo;
    public MessageManager(String[] infoMessage, String pseudo){
        this.infoMessage = infoMessage;
        this.pseudo = pseudo;
    }

    /**
     * return a buffer sized and full for a message response
     * @return ByteBuffer
     */
    public ByteBuffer get(){
        ByteBuffer buffer;
        ByteBuffer bufferPseudo;
        String cr;
        String size;
        String msg;
        switch (Integer.parseInt(infoMessage[0])){
            case(9):
                bufferPseudo = Charset.forName("UTF-8").encode(pseudo+": ");
                cr = infoMessage[0];
                size = infoMessage[2] ;
                msg = infoMessage[3];
                buffer = ByteBuffer.allocate(Integer.BYTES*2 +Integer.parseInt(size)+ bufferPseudo.remaining());
                buffer.putInt(Integer.parseInt(cr));
                buffer.putInt(Integer.parseInt(size) + bufferPseudo.remaining());
                buffer.put(bufferPseudo);
                buffer.put(Charset.forName("UTF-8").encode(msg));
                return buffer;
            case (10):
                cr = infoMessage[0];
                msg = infoMessage[3];
                ByteBuffer tampon = Charset.forName("UTF-8").encode(msg);
                buffer = ByteBuffer.allocate(Integer.BYTES + tampon.capacity());
                buffer.putInt(Integer.parseInt(cr));
                buffer.put(Charset.forName("UTF-8").encode(msg));
                return buffer;

            case(11):
                bufferPseudo = Charset.forName("UTF-8").encode(pseudo);
                cr = infoMessage[0];
                size = infoMessage[2] ;
                msg = infoMessage[3];
                buffer = ByteBuffer.allocate(Integer.BYTES*2 +Integer.parseInt(size)+ bufferPseudo.remaining());
                buffer.putInt(Integer.parseInt(cr));
                buffer.putInt(Integer.parseInt(size) + bufferPseudo.remaining());
                buffer.put(bufferPseudo);
                buffer.put(Charset.forName("UTF-8").encode(msg));
                return buffer;

            case(19):
                bufferPseudo = Charset.forName("UTF-8").encode(pseudo);
                cr = infoMessage[0];
                buffer = ByteBuffer.allocate(Integer.BYTES*3 + bufferPseudo.remaining());
                buffer.putInt(Integer.parseInt(cr));
                buffer.putInt(0);
                buffer.putInt(bufferPseudo.remaining());
                buffer.put(bufferPseudo);
                return buffer;

            case(20):
                bufferPseudo = Charset.forName("UTF-8").encode(pseudo);
                cr = infoMessage[0];
                buffer = ByteBuffer.allocate(Integer.BYTES*2 + bufferPseudo.remaining());
                buffer.putInt(Integer.parseInt(cr));
                buffer.putInt(bufferPseudo.remaining());
                buffer.put(bufferPseudo);
                return buffer;

            case(22):
                bufferPseudo = Charset.forName("UTF-8").encode(pseudo);
                cr = infoMessage[0];
                buffer = ByteBuffer.allocate(Integer.BYTES*3 + bufferPseudo.remaining());
                buffer.putInt(Integer.parseInt(cr));
                buffer.putInt(0);
                buffer.putInt(bufferPseudo.remaining());
                buffer.put(bufferPseudo);
                return buffer;
        }
        return ByteBuffer.allocate(0);
    }
}
