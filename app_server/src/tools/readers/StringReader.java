package tools.readers;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class StringReader implements Reader{
    private enum State {DONE,WAITING_INT,ERROR,WAITING_MSG};

    private final ByteBuffer bb;
    private State state = State.WAITING_INT;
    private int msgLength;
    private String msg;

    public StringReader(ByteBuffer bb) {
        this.bb = bb;
    }

    /**
     *
     * @return ProcessStatus
     */
    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }
        try {
            if (bb.limit()- bb.position() >= Integer.BYTES) {
                getSizeMessage();
                if (bb.limit()- bb.position() >= msgLength){
                    getMessage();
                    return ProcessStatus.DONE;
                }else if((bb.limit()+4)- bb.position() >= msgLength){
                    bb.limit(bb.limit()+4);
                    getMessage();
                    return ProcessStatus.DONE;
                }
                return ProcessStatus.REFILL;
            }
            return ProcessStatus.REFILL;
        } finally {
            bb.compact();
        }
    }

    private void getMessage() {
        msg = Charset.forName("UTF-8").decode(bb).toString();
        state = State.DONE;
    }

    private void getSizeMessage() {
        msgLength = bb.getInt();
    }

    @Override
    public Object get() {
        if (state!=State.DONE) {
            throw new IllegalStateException();
        }
        return msg;
    }

    @Override
    public void reset() {
        state=State.WAITING_INT;
    }

    @Override
    public int getSize() {
        return msgLength;
    }
}
