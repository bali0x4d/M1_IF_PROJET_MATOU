package tools.readers;

import java.nio.ByteBuffer;

public class RequestCodeReader {

    public static int get(ByteBuffer buffer){
        buffer.flip();
        int rc = buffer.getInt();
        buffer.compact();
        return rc;
    }
}
