package tools.readers;

import java.nio.ByteBuffer;

public class PrivateConvResponseMessageReader implements Reader{
    private enum State {DONE, WAITING_CR, WAITING_RESPONSE, WAITING_MSG, ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITING_CR;
    private Reader.ProcessStatus stateMsg = Reader.ProcessStatus.REFILL;
    private Reader.ProcessStatus stateCR = Reader.ProcessStatus.REFILL;
    private Reader.ProcessStatus stateResponse = Reader.ProcessStatus.REFILL;
    private String[] infoMessage;

    /**
     *
     * @param bb buffer containing a message
     */
    private PrivateConvResponseMessageReader(ByteBuffer bb) {
        this.bb = bb;
        this.infoMessage = new String[4];
    }

    /**
     *
     * @return ProcessStatus
     */
    @Override
    public Reader.ProcessStatus process() {
        if (stateMsg == Reader.ProcessStatus.DONE && stateResponse == Reader.ProcessStatus.DONE
                || stateMsg == Reader.ProcessStatus.ERROR && stateResponse == Reader.ProcessStatus.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        switch (state){
            case WAITING_CR:
                if (bb.remaining() >= Integer.BYTES) {
                    infoMessage[0] = String.valueOf(19);
                    stateCR = Reader.ProcessStatus.DONE;
                    state = State.WAITING_RESPONSE;
                }
            case WAITING_RESPONSE:
                if(bb.remaining() >= Integer.BYTES) {
                    int token = bb.getInt();
                    infoMessage[1] = String.valueOf(token);
                    stateResponse = Reader.ProcessStatus.DONE;
                    state = State.WAITING_MSG;
                }
            case WAITING_MSG:
                StringReader stringReader = new StringReader(bb);

                if (stateMsg == Reader.ProcessStatus.REFILL) {
                    stateMsg = stringReader.process();
                }

                if (stateMsg == Reader.ProcessStatus.DONE && state == State.WAITING_MSG) {
                    infoMessage[2] = String.valueOf(stringReader.getSize());
                    infoMessage[3] = (String) stringReader.get();
                    state = State.DONE;
                    return Reader.ProcessStatus.DONE;
                }
        }
        return Reader.ProcessStatus.REFILL;
    }

    @Override
    public Object get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return infoMessage;
    }

    @Override
    public void reset() {
        state = State.WAITING_CR;
        stateResponse = Reader.ProcessStatus.REFILL;
        stateCR = Reader.ProcessStatus.REFILL;
        stateMsg = Reader.ProcessStatus.REFILL;
    }

    @Override
    public int getSize() {
        return Integer.parseInt(infoMessage[2]);
    }

    public static PrivateConvResponseMessageReader createPrivateConvResponseMessageReader(ByteBuffer buffer){
        return new PrivateConvResponseMessageReader(buffer);
    }
}
