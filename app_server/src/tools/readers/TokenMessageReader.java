package tools.readers;


import java.nio.ByteBuffer;

public class TokenMessageReader implements Reader {
    private enum State {DONE, WAITING_CR, WAITING_TOKEN, WAITING_MSG, ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITING_CR;
    private ProcessStatus stateMsg = ProcessStatus.REFILL;
    private ProcessStatus stateCR = ProcessStatus.REFILL;
    private ProcessStatus stateToken = ProcessStatus.REFILL;
    private String[] infoMessage;

    /**
     *
     * @param bb buffer containing a message
     */
    private TokenMessageReader(ByteBuffer bb) {
        this.bb = bb;
        this.infoMessage = new String[4];
    }

    /**
     *
     * @return ProcessStatus
     */
    @Override
    public Reader.ProcessStatus process() {
        if (stateMsg == Reader.ProcessStatus.DONE && stateToken == Reader.ProcessStatus.DONE
                || stateMsg == Reader.ProcessStatus.ERROR && stateToken == Reader.ProcessStatus.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        switch (state){
            case WAITING_CR:
                if (bb.remaining() >= Integer.BYTES) {
                    infoMessage[0] = String.valueOf(9);
                    stateCR = ProcessStatus.DONE;
                    state = State.WAITING_TOKEN;
                }
            case WAITING_TOKEN:
                if(bb.remaining() >= Integer.BYTES) {
                    int token = bb.getInt();
                    infoMessage[1] = String.valueOf(token);
                    stateToken = ProcessStatus.DONE;
                    state = State.WAITING_MSG;
                }
            case WAITING_MSG:
                StringReader stringReader = new StringReader(bb);

                if (stateMsg == Reader.ProcessStatus.REFILL) {
                    stateMsg = stringReader.process();
                }

                if (stateMsg == Reader.ProcessStatus.DONE && state == State.WAITING_MSG) {
                    infoMessage[2] = String.valueOf(stringReader.getSize());
                    infoMessage[3] = (String) stringReader.get();
                    state = State.DONE;
                    return Reader.ProcessStatus.DONE;
                }
        }
        return ProcessStatus.REFILL;
    }

    @Override
    public Object get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return infoMessage;
    }

    @Override
    public void reset() {
        state = State.WAITING_CR;
        stateToken = ProcessStatus.REFILL;
        stateCR = ProcessStatus.REFILL;
        stateMsg = ProcessStatus.REFILL;
    }

    @Override
    public int getSize() {
        return Integer.parseInt(infoMessage[2]);
    }

    public static TokenMessageReader createTokenMessageReader(ByteBuffer buffer){
        return new TokenMessageReader(buffer);
    }
}
