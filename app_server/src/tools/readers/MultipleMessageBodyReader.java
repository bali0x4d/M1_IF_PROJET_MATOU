package tools.readers;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class MultipleMessageBodyReader implements Reader{
    private enum State {DONE, WAITING_CR, WAITING_TOKEN, WAITING_MSG, ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITING_CR;
    private Reader.ProcessStatus stateMsg = Reader.ProcessStatus.REFILL;
    private Reader.ProcessStatus stateToken = Reader.ProcessStatus.REFILL;
    private ProcessStatus stateCR = Reader.ProcessStatus.REFILL;
    private String[] infoMessage;

    private MultipleMessageBodyReader(ByteBuffer bb) {
        this.bb = bb;
        this.infoMessage = new String[4];
    }

    @Override
    public Reader.ProcessStatus process() {

        if (stateMsg == Reader.ProcessStatus.DONE && stateToken == Reader.ProcessStatus.DONE
                || stateMsg == Reader.ProcessStatus.ERROR && stateToken == Reader.ProcessStatus.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();

        switch (state){
            case WAITING_CR:
                if (bb.remaining() >= Integer.BYTES) {
                    infoMessage[0] = String.valueOf(10);
                    stateCR = ProcessStatus.DONE;
                    state = State.WAITING_TOKEN;
                    System.out.println(state);
                }
            case WAITING_TOKEN:
                if(bb.remaining() >= Integer.BYTES) {
                    int token = bb.getInt();
                    infoMessage[1] = String.valueOf(token);
                    stateToken = ProcessStatus.DONE;
                    state = State.WAITING_MSG;
                    System.out.println("TOKEN DONE " +Integer.parseInt(infoMessage[1]));
                    System.out.println(state);
                }
            case WAITING_MSG:

                if (state == State.WAITING_MSG) {
                    System.out.println("MESSAGE DONE");
                    infoMessage[2] = "1024";
                    infoMessage[3] = Charset.forName("UTF-8").decode(bb).toString();
                    System.out.println("MESSAGE :"+ infoMessage[3]);
                    state = State.DONE;
                    System.out.println(state);
                    return Reader.ProcessStatus.DONE;
                }
        }
        return ProcessStatus.REFILL;
    }

    @Override
    public Object get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return infoMessage;
    }

    @Override
    public void reset() {
        state = State.WAITING_CR;
    }

    @Override
    public int getSize() {
        return 0;
    }

    public static MultipleMessageBodyReader createMultipleMessageBodyReader(ByteBuffer buffer){
        return new MultipleMessageBodyReader(buffer);
    }
}
