package tools.readers;

public interface Reader {

    enum ProcessStatus {DONE,REFILL,ERROR}

    /**
     * try to get all information contained in the input buffer
     * @return enum status
     */
    ProcessStatus process();

    /**
     * return what was on the input buffer
     * @return Object
     */
    Object get();

    /**
     * reset the enum status
     */
    void reset();

    /**
     * Return the size of the message
     * @return message size
     */
    int getSize();


}
